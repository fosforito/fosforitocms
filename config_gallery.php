<?php

#################  MyGallery - Configuration File  ###################

  ###### Section: MySQL-Database Details ######
  //Database-server, generally it is localhost
  $server    = "localhost"; 
  
  // Database Name, must already exist
  $database  = ""; 
  
  //The username for the Database
  $user      = ""; 
  
  //The Password for the Database
  $password  = ""; 	
  
  //Root Directory - Absolute Path to your Website
  $absolute_path = "";
  
###################################################################### 

//Connecting to Database
  @mysql_connect("$server", "$user", "$password") or die('<h1>Cannot connect to MySQL Server...</h1>');
  @mysql_select_db("$database") or die('<h1>Cannot connect to Database...</h1>'); 
  
?>