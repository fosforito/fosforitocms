<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

if($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass AND $user_level == 1){ 
    
    if(isset($_GET['action'])){
        $action = $_GET['action'];
    } else {
        $action = "all";
    }

    if($action == "new"){
        
        //Load
        require('./sources/new_blog.php');
        
    } else {
        
        //Load
        require('./sources/gal_blog.php');
        
    }
}

?>
