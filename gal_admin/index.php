<?php
session_start();

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

################ Component: Administration Index #################

//Load Configuration...
  require("../config_gallery.php");
  require("../gallery_includes/get_gal_conf.php");

//Load Language-Pack
  require("../gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_addpic_script.php");

//Set default Session if it does not already exist
  if(!isset($_SESSION['admin_name'])){
    $_SESSION['admin_name'] = "";
  }
  if(!isset($_SESSION['admin_pass'])){
    $_SESSION['admin_pass'] = "";
  }

//Get Session Variables
  $user_name = @$_SESSION['user_name'];
  $user_pass = @$_SESSION['user_pass'];
  $user_level = @$_SESSION['user_level'];

//Verificate User Details
  include('../gallery_includes/user_ver.php'); 

//Load Template
  require('./sources/index.php');

//Close MySQL Connection
  mysql_close();

?>
