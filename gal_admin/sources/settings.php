<?php
 
/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Load Language-Pack
  require("../gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_addpic_script.php");
  
$show_message_ok = 0;
$show_message_ok_message = '<h4 style="color:green;">Settings Updated!</h4>';
  
// Update Configuration 
if(isset($_POST['set'])){
$set = $_POST['set'];

if($set == 2) {

$pics_per_row = $_POST['pics_per_row'];
$pics_per_page = $_POST['pics_per_page'];
$set_order_by = $_POST['set_order_by'];
$show_klicks = $_POST['show_klicks'];
$show_dats = $_POST['show_dats'];
$gal_maxfilesize = $_POST['gal_maxfilesize'];
$set_image_maxheight = $_POST['set_image_maxheight'];
$set_image_maxwidth = $_POST['set_image_maxwidth'];

     $query = "UPDATE gal_conf SET conf_value = '$pics_per_row' WHERE conf_name = 'pics_per_row'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$pics_per_page' WHERE conf_name = 'pics_per_page'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$set_order_by' WHERE conf_name = 'set_order_by'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$show_klicks' WHERE conf_name = 'show_klicks'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$show_dats' WHERE conf_name = 'show_dats'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$gal_maxfilesize' WHERE conf_name = 'gal_maxfilesize'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$set_image_maxheight' WHERE conf_name = 'set_image_maxheight'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$set_image_maxwidth' WHERE conf_name = 'set_image_maxwidth'";
     mysql_query($query);
	
	 $show_message_ok = 1;

} elseif($set == 3) {
    
$set_stylesheet = $_POST['set_stylesheet'];
$site_width = $_POST['site_width'];
$set_admin_bar = $_POST['set_admin_bar'];

     $query = "UPDATE gal_conf SET conf_value = '$set_stylesheet' WHERE conf_name = 'set_stylesheet'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$site_width' WHERE conf_name = 'site_width'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$set_admin_bar' WHERE conf_name = 'set_admin_bar'";
     mysql_query($query);
	 
	 $show_message_ok = 1;

} elseif($set == 1) {
    
$set_url = $_POST['set_url'];
$set_site_language = $_POST['set_site_language'];
$set_site_name = $_POST['set_site_name'];
$set_site_slogan = $_POST['set_site_slogan'];
$absolute_path = $_POST['absolute_path'];
	 
     $query = "UPDATE gal_conf SET conf_value = '$set_url' WHERE conf_name = 'set_url'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$set_site_language' WHERE conf_name = 'set_site_language'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$set_site_name' WHERE conf_name = 'set_site_name'";
     mysql_query($query);
	 
     $query = "UPDATE gal_conf SET conf_value = '$absolute_path' WHERE conf_name = 'absolute_path'";
     mysql_query($query);
     
     $query = "UPDATE gal_conf SET conf_value = '$set_site_slogan' WHERE conf_name = 'set_site_slogan'";
     mysql_query($query);

	 $show_message_ok = 1;
         
} elseif($set == 4) {
    
$set_allow_regs = $_POST['set_allow_regs'];

     $query = "UPDATE gal_conf SET conf_value = '$set_allow_regs' WHERE conf_name = 'allow_regs'";
     mysql_query($query);
     
         $show_message_ok = 1;
     
}
}
// End Update Configuration


if(isset($_GET['tab'])){
    $get_set_var = $_GET['tab'];   // Get tab number from URL if is set
} else {
    $get_set_var = "1";            // Set default tab if no tab was selected in URL
}


if($get_set_var == 2){
#Tab2

      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'pics_per_row'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v1 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'pics_per_page'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v2 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_order_by'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v3 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'show_klicks'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v4 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'show_dats'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v5 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'gal_maxfilesize'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v6 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_image_maxheight'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v7 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_image_maxwidth'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
      
          $v8 = $obj->conf_value;
      
      switch ($v3) {
          case 'ASC':
              $v31 = "Ascending";
              break;
          case 'DESC':
              $v31 = "Descending";
              break;
      }
      
      switch ($v4) {
          case 'yes':
              $v41 = "Yes";
              break;
          case 'no':
              $v41 = "No";
              break;
      }
      
      switch ($v5) {
          case 'yes':
              $v51 = "Yes";
              break;
          case 'no':
              $v51 = "No";
              break;
      }
	

echo '
<span class="admin_pad"><a href="index.php?c=settings.php&tab=1">General</a></span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=4">Permissions</a></span>
<span class="admin_pad_on">Gallery</span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=3">Styles</a></span>
<br/>';

if($show_message_ok == 1){
    echo $show_message_ok_message;
}else{
    echo '<br/>';
}

echo '
<form action="index.php?c=settings.php&set=2&tab=2" method="post">
<input type="hidden" name="set" value="2"></input>
<table align="center" border="0">
<tr>
<td align="right">
</td>
</tr>
<tr>
<td align="right">
Pics per Row:
</td>
<td align="left">
<input type="text" name="pics_per_row" value="'.$v1.'"></input>
</td>
</tr>
<tr>
<td align="right">
Pics per Page:
</td>
<td align="left">
<input type="text" name="pics_per_page" value="'.$v2.'"></input>
</td>
</tr>
<tr>
<td align="right">
Order Images:
</td>
<td align="left">
<select name="set_order_by">
<option value="'.$v3.'">'.$v31.'</option>
<option value="ASC">Ascending</option>
<option value="DESC">Descending</option>
</select>
</td>
</tr>
<tr>
<td align="right">
Show clicks:
</td>
<td align="left">
<select name="show_klicks">
<option value="'.$v4.'">'.$v41.'</option>
<option value="yes">Yes</option>
<option value="no">No</option>
</select>
</td>
</tr>
<tr>
<td align="right">
Show Title & Description:
</td>
<td align="left">
<select name="show_dats">
<option value="'.$v5.'">'.$v51.'</option>
<option value="yes">Yes</option>
<option value="no">No</option>
</select>
</td>
</tr>
<tr>
<td align="right">
Maximal Image size:
</td>
<td align="left">
<input type="text" name="gal_maxfilesize" value="'.$v6.'"></input>
</td>
</tr>
<tr>
<td align="right">
Maximal Image Height:
</td>
<td align="left">
<input type="text" name="set_image_maxheight" value="'.$v7.'"></input>
</td>
</tr>
<tr>
<td align="right">
Maximal Image Width:
</td>
<td align="left">
<input type="text" name="set_image_maxwidth" value="'.$v8.'"></input>
</td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" value="Submit"></input>
</td>
</tr>
</table>
</form>
';

}

if($get_set_var == 3){
#Tab3

      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_stylesheet'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v1 = $obj->conf_value;
          
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'site_width'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v2= $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_admin_bar'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v3= $obj->conf_value;
          
      switch ($v3) {
          case 'yes':
              $v31 = "Yes";
              break;
          case 'no':
              $v31 = "No";
              break;
      }

echo '
<span class="admin_pad"><a href="index.php?c=settings.php&tab=1">General</a></span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=4">Permissions</a></span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=2">Gallery</a></span>
<span class="admin_pad_on">Styles</span>
<br/>';

if($show_message_ok == 1){
    echo $show_message_ok_message;
}else{
    echo '<br/>';
}

echo '
<form method="post" action="index.php?c=settings.php&set=3&tab=3">
<input type="hidden" name="set" value="3"></input>
<table align="center" border="0">
<tr>
<td align="right">
Admin Stylesheet:
</td>
<td align="left">
<input type="text" name="set_stylesheet" value="'.$v1.'"></input>
</td>
</tr>
<tr>
<td align="right">
Enable Admin Bar:
</td>
<td align="left">
<select name="set_admin_bar">
<option value="'.$v3.'">'.$v31.'</option>
<option value="yes">Yes</option>
<option value="no">No</option>
</select>
</td>
</tr>
<tr>
<td align="right">
Admin Panel Width:
</td>
<td align="left">
<input type="text" name="site_width" value="'.$v2.'"></input>
</td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" value="Submit"></input>
</td>
</tr>
</table>
</form>
';

}elseif($get_set_var == 1){
#Tab4

      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_site_language'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v2 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_url'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v3 = $obj->conf_value;
	  
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_site_name'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v4 = $obj->conf_value;
          
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'set_site_slogan'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v5 = $obj->conf_value;
          
      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'absolute_path'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v6 = $obj->conf_value;
          
          
          switch ($v2) {
              case 'en':
                  $v21 = "English";
                  break;
              case 'es':
                  $v21 = "Espa&ntilde;ol";
                  break;
              case 'de':
                  $v21 = "Deutsch";
                  break;
          }

echo '
<span class="admin_pad_on">General</span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=4">Permissions</a></span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=2">Gallery</a></span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=3">Styles</a></span>
<br/>';

if($show_message_ok == 1){
    echo $show_message_ok_message;
}else{
    echo '<br/>';
}

echo '
<form action="index.php?c=settings.php&set=1&tab=1" method="post">
<input type="hidden" name="set" value="1"></input>
<table align="center" border="0">


<tr>
<td align="right">
Site Name:
</td>
<td align="left">
<input type="text" name="set_site_name" value="'.$v4.'"></input>
</td>
</tr>
<tr>
<td align="right">
Site Slogan:
</td>
<td align="left">
<input type="text" name="set_site_slogan" value="'.$v5.'"></input>
</td>
</tr>
<tr>
<td align="right">
Site Language:
</td>
<td align="left">
<select name="set_site_language">
<option value="'.$v2.'">'.$v21.'</option>
<option value="en">English</option>
<option value="es">Espa&ntilde;ol</option>
<option value="de">Deutsch</option>
</select>
</td>
</tr>

<tr>
<td align="right">
Path to FosforitoCMS:
</td>
<td align="left">
<input type="text" name="set_url" value="'.$v3.'"></input>
</td>
</tr>

<tr>
<td align="right">
Absolute Path:
</td>
<td align="left">
<input type="text" name="absolute_path" value="'.$v6.'"></input>
</td>
</tr>

<tr>
<td colspan="2" align="center">
<input type="submit" value="Submit"></input>
</td>
</tr>
</table>
</form>
';

} elseif($get_set_var == 4) {
    
    // Permissions Config -------------------------------------------------------------------------------------
    #Tab3

      $query = "SELECT conf_value FROM gal_conf WHERE conf_name = 'allow_regs'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
	  $v1 = $obj->conf_value;
          
      switch ($v1) {
          case '1':
              $v11 = "Yes";
              break;
          default:
              $v11 = "No";
              break;
      }

echo '
<span class="admin_pad"><a href="index.php?c=settings.php&tab=1">General</a></span>
<span class="admin_pad_on">Permissions</span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=2">Gallery</a></span>
<span class="admin_pad"><a href="index.php?c=settings.php&tab=3">Styles</a></span>
<br/>';

if($show_message_ok == 1){
    echo $show_message_ok_message;
}else{
    echo '<br/>';
}

echo '
<form method="post" action="index.php?c=settings.php&set=4&tab=4">
<input type="hidden" name="set" value="4"></input>
<table align="center" border="0">
<tr>
<td align="right">
Allow User Registration:
</td>
<td align="left">
<select name="set_allow_regs">
<option value="'.$v1.'">'.$v11.'</option>
<option value="1">Yes</option>
<option value="0">No</option>
</select>
</td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" value="Submit"></input>
</td>
</tr>
</table>
</form>
';

}

?>