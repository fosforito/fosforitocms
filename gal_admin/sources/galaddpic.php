<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Add Editor to textarea  
  require("../gallery_includes/editorheader.php");
  
//Load Language-Pack
  require("../gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_addpic.php");


echo '
<form enctype="multipart/form-data" action="index.php?c=galimages.php&action=add2" method="post">
<table align="center">
        <tr>
          <td align="right" valign="top"><b>';
		  
echo $_e['lg_addpic_file'];
 
echo '</b></td>
          <td align="left" valign="top">
            <input type="file" name="img_file"><a title="Select a Image to upload (jpg or gif)">
<img src="../gallery_includes/images/ask1.jpg"></img></a>
          </td>
        </tr>
		<tr>
          <td align="right" valign="top"><b>';
		  
echo $_e['lg_addpic_text'];
         
echo '</b></td>
          <td align="left" valign="top">
            <input type="text" name="img_title" size="37" maxlength="50"><a title="The Title of your Image">
<img src="../gallery_includes/images/ask1.jpg"></img></a>
          </td>
       </tr>	
       <tr>
          <td align="right" valign="top"><b>
             '.$_e["lg_addpic_descr"].'
          </b></td>
          <td align="left" valign="top">
             <textarea id="simple" name="pic_descr" rows="5" cols="30"></textarea>
          </td>
       </tr>		
		<tr>
          <td colspan="2" align="center" valign="top">
            <input type="submit" value="';

echo $_e['lg_addpic_submit'];
			
echo '">
          </td>
        </tr>
      </table>
  </form>';

//Add Editor to textarea  
  require("../gallery_includes/editorfooter.php");
  
?>