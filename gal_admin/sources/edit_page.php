<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Add Editor to textarea  
  require("../gallery_includes/editorheader.php");

$time = time();

if(isset($_POST['new'])){
//Add new Page

$page_title = $_POST['page_title'];
$page_title = str_replace("'", "''", $page_title);
$page_url = $_POST['page_url'];
$page_url = str_replace(" ", "_", $page_url);
$pagecontent = $_POST['page_content'];
$pagecontent = str_replace("'", "''", $pagecontent);
$pagestatuss = $_POST['status']; //Statuss is correct!

$rand_number = rand(1, 99999);

if($page_title == "" OR $page_title == " "){
$page_title = $rand_number;
}

if($page_url == "" OR $page_title == " "){
$page_url = $rand_number;
}

$query = "INSERT INTO gal_pages (page_title, page_url, page_date, page_lupdate, page_content, page_status) VALUES ('$page_title', '$page_url', '$time', '$time', '$pagecontent', '$pagestatuss')";
mysql_query($query);

echo '<h4 style="color:green;">Page added!</h4>';
}


if(isset($_GET['edit']) AND !isset($_POST['edit'])){
     $page_url = $_GET['edit'];
     }	

if(isset($_POST['edit'])){

//Update Page Content

$update_page = $_POST['edit'];
$pagecontent = $_POST['page_content'];
$pagecontent = str_replace("'", "''", $pagecontent);
$up_page_title = $_POST['up_page_title'];
$up_page_title = str_replace("'", "''", $up_page_title);
$up_page_url = $_POST['up_page_url'];
$up_page_url = str_replace(" ", "_", $up_page_url);
$status = $_POST['status'];

$rand_number2 = rand(1, 99999);

if($up_page_title == ""){
$up_page_title = $rand_number2;
}

if($up_page_url == "" OR $up_page_title == " "){
$up_page_url = $rand_number2;
}

	  $query = "UPDATE gal_pages SET page_content = '$pagecontent' WHERE page_url = '$update_page'";
      mysql_query($query);
	  
	  $query = "UPDATE gal_pages SET page_lupdate = '$time' WHERE page_url = '$update_page'";
      mysql_query($query);
	  
	  $query = "UPDATE gal_pages SET page_title = '$up_page_title' WHERE page_url = '$update_page'";
      mysql_query($query);
	  
	  $query = "UPDATE gal_pages SET page_url = '$up_page_url' WHERE page_url = '$update_page'";
      mysql_query($query);
	  
	  $query = "UPDATE gal_pages SET page_status = '$status' WHERE page_url = '$update_page'";
      mysql_query($query);


echo '<h4 style="color:green;">Page updated!</h4>';
$page_url = $up_page_url;
}

//View Textarea-Editor to edit Page
//Getting Page-Content...

	 
	       $query = "SELECT * FROM gal_pages WHERE page_url = '$page_url'";
           $result = mysql_query($query);
           $obj = mysql_fetch_object($result);
	  
$editpagecontent = $obj->page_content;
$editpage_title = $obj->page_title;
$editpage_url = $obj->page_url; 
$editstatus = $obj->page_status;

echo'
<table align="center" border="0"><tr><td>
<table align="left" border="0"><tr><td>
<form action="index.php?c=pages.php&action=edit&edit='.$page_url.'" method="post">
<input type="hidden" name="edit" value="'.$page_url.'"></input>
<div align="left">
<b>Page Title:</b> <input type="text" name="up_page_title" size="50" value="'.$editpage_title.'"></input><br/>
<b>Page URL:</b> <input type="text" name="up_page_url" size="50" value="'.$editpage_url.'"></input><br/><small>URL Example: my_gallery_tut (<b>no special caracters or spaces!</b>)</small><br/><br/>
</div>
<textarea name="page_content" id="advanced">'.$editpagecontent.'</textarea><br/>
<b>Status:</b> <select name="status">
<option value="'.$editstatus.'">'.$editstatus.'</option>
<option value="active">Active</option>
<option value="inactive">Inactive</option>
</select><br/>
<center><input type="submit" value="Update"></input>
</form><br/></center>
</td></tr></table>
</td></tr></table>
';

//Add Editor to textarea  
  require("../gallery_includes/editorfooter.php");
  
?>