<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/


echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

	<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
  <head>  
    <title>
      '.get_gal_conf('set_site_name').' | Admin Center
    </title>';
	

//Load CSS-----------------------------------------------------------------
   echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gal_admin/sources/styles/'.get_gal_conf('set_stylesheet').'" />';	
   echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/fonts/icomoon/style.css"/>';
   echo '</head><body onload="java script:timer()"> ';
   
//Admin Bar Function-------------------------------------------------------     
//Place Admin Bar----------------------------------------------------------

if(get_gal_conf('set_admin_bar') == "yes"){   
   echo '<div id="status_bar">';
//Content of Admin Bar  --------------------------------------------------- 
   echo '<table width="98%" border="0">
        <tr>
        <td>
        <span aria-hidden="true" data-icon="&#xe039;"></span> <b>Hello '.$_SESSION['user_name'].'!</b>
        </td>
        <td align="right">Welcome!</td>
        </tr>
        </table>';   
   echo '</div>';    
   $extra = ' style="margin-top: 35px"';
} else {
   $extra = "";
}


echo '
  <table width="'.get_gal_conf("site_width").'" border="0" align="center">
<tr>
<td>
<div align="center" class="complete_table"'.$extra.'>';

 if($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass AND $user_level == 1){ 

echo '
<table width="100%" border="0" valign="top">
<tr>
<td colspan="2"><div class="little_table"><font size="5"><b>&nbsp;&nbsp;<span aria-hidden="true" data-icon="&#xe00a;">&nbsp;Administration Panel</span></b></font></div></td>
<tr>
<td width="300px" align="left" valign="top">';

//-------------------------
//MySQL Connect: NAVIGATION
//-------------------------

$query = "SELECT * FROM gal_admin WHERE tab = '1'";
$result = mysql_query($query);

$num = mysql_numrows($result);
echo '<div class="little_table" style="padding-left:0px; padding-top:10px; padding-right:0px; padding-bottom:10px;">
<table width="150px" border="0" valign="top">
<tr><td>
<div>

<div id="menuvertical">
<ul>';

$i=0;//Start Counter and show outputs from While-Loop----------------------------
while ($i < $num) {

$cont_ico = mysql_result($result,$i,"cont_ico");
$cont_nav = mysql_result($result,$i,"cont_nav");
$cont_dat = mysql_result($result,$i,"cont_dat");

echo '<li><a href="index.php?c='.$cont_dat.'" style="padding-left:10px;"><b><span aria-hidden="true" data-icon="'.$cont_ico.'"></span> '.$cont_nav.'</b></a>';


$query2 = "SELECT * FROM gal_admin WHERE tab = '2' AND sub_dat = '".$cont_dat."' AND sub_nav = '".$cont_nav."'";
$result2 = mysql_query($query2);
$num2 = mysql_numrows($result2);
$i2 = 0;

if($num2 >= 1){
echo '<ul>';
while ($i2 < $num2) {

$cont_nav2 = mysql_result($result2,$i2,"sub_cont_nav");
$cont_dat2 = mysql_result($result2,$i2,"sub_cont_dat");

echo '<li><a href="index.php?c='.$cont_dat2.'" style="padding-left:10px;"><b>'.$cont_nav2.'</b></a>';

$i2++;
}
echo '</ul>';
}


echo '</li>';


$i++;
}//Close While-Loop-------------------------------------------------------------

echo '</ul>
</div></div></td></tr></table></div>';

echo '</td>
<td width="100%" align="left" valign="top">
<div class="little_table2">
<table width="100%" border="0">
<tr>
<td>';

//----------------------
//MySQL Connect: CONTENT
//----------------------

if(!isset($_GET['c'])){
$cont_dat = 'admin_index.php';
} else {
$cont_dat = $_GET["c"];
}
require("./".$cont_dat);


echo '</td>
</tr>
</table>
</div>
</td>
</tr>
</table>';  
  
}else{
echo $_e['lg_addpic_script_loginfirst'];
} 

//Load Footer-------------------------------------------------------------
require("../gallery_includes/themes/gal_default/footer2.php");

	echo '</div>
</td></tr></table>
  </body>
</html>';

?>