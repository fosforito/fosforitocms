<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Load Language-Pack
  require("../gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_addpic_script.php");
  
/*Add a random Number to the Imagename, so it's possible to upload the same Image with
different styles and the existing Image will not be over written.*/
$rand_number = rand(1,999999999);
$file_path = get_gal_conf('file_path');
  
  $tempname = $_FILES['img_file']['tmp_name'];
  $name = "$rand_number".$_FILES['img_file']['name'];
  $type = $_FILES['img_file']['type'];
  $size = $_FILES['img_file']['size'];
  $descr  = $_POST['pic_descr'];
  $titlei  = $_POST['img_title'];
  $descr = str_replace("'", "''", $descr);

  if($type != "image/gif" && $type != "image/jpeg" && $type != "image/png")
    {
      $err[] = $_e["lg_addpic_script_extensionerror"];
    }

  if($size > get_gal_conf('gal_maxfilesize'))
    {
      $err[] = $_e["lg_addpic_script_overmaxsize"];
    }

	
	if(empty($err))
    {
      //Copy Temp-file to File-directory
      if(@copy("$tempname", "../$file_path$name"))
        echo $_e["lg_addpic_script_uploaded"];
      else
        $err[] = $_e["lg_addpic_script_notuploaded"];
    }
	
	
	if(empty($err))
    {
      $timestamp = time();
      $query = "INSERT INTO gal_pics (pic_dat, pic_date, pic_descr, pic_title) VALUES ('$name', '$timestamp', '$descr', '$titlei')";

      if(@mysql_query($query))
        echo $_e["lg_addpic_script_addedtodb"];
      else
        $err[] = $_e["lg_addpic_script_notaddedtodb"];
    }
	

	
	if(empty($err))
    {
      if($type == "image/gif")
        {
          $picture = imagecreatefromgif('../'.$file_path.$name);

          if($picture)
            echo $_e["lg_addpic_script_loaded"];
       else
             $err[] = $_e["lg_addpic_script_notloaded"];
        }
    }
	
	
	if(empty($err))
    {
      if($type == "image/jpeg")
        {
          $picture = imagecreatefromjpeg('../'.$file_path.$name);

          if($picture)
            echo $_e["lg_addpic_script_loaded"];
       else
             $err[] = $_e["lg_addpic_script_notloaded"];
        }
    }
	
	
	
	
	if(empty($err))
    {
      if($type == "image/png")
        {
          $picture = imagecreatefrompng('../'.$file_path.$name);

          if($picture)
            echo $_e["lg_addpic_script_loaded"];
       else
             $err[] = $_e["lg_addpic_script_notloaded"];
        }
    }
	
	
	
	
	if(empty($err))
    {
      $imgdatas = getimagesize('../'.$file_path.$name);

      if($imgdatas)
        {
          $orig_width = $imgdatas[0];
          $orig_height = $imgdatas[1];

          $new_width = 100;
          $new_height = $new_width / ($orig_width / $orig_height);

          echo $_e["lg_addpic_script_getsize"];
        }
      else
        $err[] = $_e["lg_addpic_script_getsizefailed"];
    }

//Create Thumbnail	
	
	if(empty($err))
    {
      $thumbnail = imagecreatetruecolor($new_width, $new_height);
      $cpy = imagecopyresized($thumbnail, $picture, 0, 0, 0, 0, $new_width, $new_height, $orig_width, $orig_height);

      if($thumbnail && $cpy)
        echo $_e["lg_addpic_script_thumbok"];
      else
        $err[] = $_e["lg_addpic_script_thumbnn"];
    }
	
	
	if(empty($err))
    {
      $quality = 100;
      $sav = imagejpeg($thumbnail, "../".$file_path."thumb_".$name, $quality);

      if($sav)
        echo $_e["lg_addpic_script_thumbd"];
      else
        $err[] = $_e["lg_addpic_script_thumbn"];
    }
	
//Create Border of Image
	
	if(empty($err) AND get_gal_conf('advanced_upload_mode') == "yes")
    {
      $color = imagecolorallocate($picture, 0, 0, 0);//Set color of Image-Border

      if($_POST['img_border_top'])
        {
          $bd_perc = 1;
          $border_height = ($orig_height * $bd_perc) / 100;
          $rect = imagefilledrectangle($picture, 0, 0, $orig_width, $border_height, $color);
        }
		
		
		if($_POST['img_border_bottom'])
        {
          $bd_perc = 10;
          $border_height = ($orig_height * $bd_perc) / 100;
          $rect = imagefilledrectangle($picture, 0, $orig_height-$border_height, $orig_width, $orig_height, $color);
        }
      if($_POST['img_border_left'])
        {
          $bd_perc = 1;
          $border_width = ($orig_width * $bd_perc) / 100;
          $rect = imagefilledrectangle($picture, 0, 0, $border_width, $orig_height, $color);
        }
      if($_POST['img_border_right'])
        {
          $bd_perc = 1;
          $border_width = ($orig_width * $bd_perc) / 100;
          $rect = imagefilledrectangle($picture, $orig_width-$border_width, 0, $orig_width, $orig_height, $color);
        }
		
		
		$quality = 100;
      $sav_bd = imagejpeg($picture, '../'.$file_path.$name, $quality);

      if($rect && $sav_bd)
        echo $_e["lg_addpic_script_bordok"];
      else
        $err[] = $_e["lg_addpic_script_bord"];
    }
	
	
	if(get_gal_conf('advanced_upload_mode') == "yes")
    {
      if($_POST['img_title'])
        {
          $text_title = $_POST['img_title'];
          $text_size = $_POST['img_title_size'];
          $text_font = $_POST['img_title_font'];
          $text_color = $_POST['img_title_color'];
          $text_X = $_POST['img_title_x'];
          $text_Y = $_POST['img_title_y'];

          if($text_color == "black")
            $color = imagecolorallocate($picture, 0, 0, 0);
          if($text_color == "white")
            $color = imagecolorallocate($picture, 255, 255, 255);
          if($text_color == "blue")
            $color = imagecolorallocate($picture, 0, 0, 255);
          if($text_color == "yellow")
            $color = imagecolorallocate($picture, 255, 255, 0);
          if($text_color == "green")
            $color = imagecolorallocate($picture, 0, 255, 0);
			
			
			 imagettftext($picture, $text_size, 0, $text_X, $text_Y, $color, $text_font, $text_title);

          $quality = 100;
          $sav_bd = @imagejpeg($picture, '../'.$file_path.$name, $quality);
        }
    }
	
	
	if(!empty($err))
   {
           foreach($err as $error)
            {
              echo "$error<br/>";
            }
      }
	echo '<center>';  
	echo $_e['lg_addpic_script_links'];
	echo '</center>';
	
	?>