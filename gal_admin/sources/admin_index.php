
<?php 

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

echo '<h3>Dashboard</h3><hr width="80%"/>';
?>


<table width="500px" border="0" align="center">
<tr>
<td align="center">
<a href="index.php?c=gal_stats.php">
<table border="0" class="imgadmin" width="120px" align="center"><tr><td align="center">
<img src="../gallery_includes/images/gstats.png" width="50" height="50"></img>
</td></tr>
<tr>
<td align="center">
<small><b>Statistics</b></small>
</td></tr>
</table>
</a>
</td>
<td align="center">
<a href="index.php?c=gal_blog.php">
<table border="0" class="imgadmin" width="120px" align="center"><tr><td align="center">
<img src="../gallery_includes/images/gblog.png" width="50" height="50"></img>
</td></tr><tr>
<td align="center">
<small><b>Blog Manager</b></small>
</td></tr>
</table>
</a>
</td>
<td align="center">
<a href="index.php?c=gal_users.php">
<table border="0" class="imgadmin" width="120px" align="center"><tr><td align="center">
<img src="../gallery_includes/images/gusers.png" width="50" height="50"></img>
</td></tr><tr>
<td align="center">
<small><b>User Manager</b></small>
</td></tr>
</table>
</a>
</td>
<td align="center">
<a href="index.php?c=settings.php">
<table border="0" class="imgadmin" width="120px" align="center"><tr><td align="center">
<img src="../gallery_includes/images/gsettings.png" width="50" height="50"></img>
</td></tr><tr>
<td align="center">
<small><b>Configuration</b></small>
</td></tr>
</table>
</a>
</td>
</tr>
</table>

<hr width="80%"/>

<br/>
<br/>





<b><small>MyGallery News</small></b>

<script type="text/javascript">

var xmlHttpObject = false;

if (typeof XMLHttpRequest != 'undefined') 
{
    xmlHttpObject = new XMLHttpRequest();
}
if (!xmlHttpObject) 
{
    try 
    {
        xmlHttpObject = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e) 
    {
        try 
        {
            xmlHttpObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch(e) 
        {
            xmlHttpObject = null;
        }
    }
}

function loadContent()
{
    xmlHttpObject.open('get','includes/loadrss.php');
    xmlHttpObject.onreadystatechange = handleContent;
    xmlHttpObject.send(null);
    return false;
}

function handleContent()
{
    if (xmlHttpObject.readyState == 4)
    {
        document.getElementById('myContent').innerHTML = xmlHttpObject.responseText;
    }
}
</script>

<p id="myContent">
    <a href="#"  style="margin-left:20px" onclick="loadContent();"><i>Display latest News from MyGallery</i></a>.
</p>







