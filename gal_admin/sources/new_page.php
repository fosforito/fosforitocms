<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Add Editor to textarea  
  require("../gallery_includes/editorheader.php");

//View Textarea-Editor to edit Page

echo'
<table align="center" border="0">
<tr>
<td>
<table align="left" border="0">
<tr>
<td>
<form action="index.php?c=pages.php&action=edit" method="post">
<input type="hidden" name="new" value="1"></input>
<b>Page Title:</b> <input type="text" name="page_title" size="35"></input><br/>
<b>Page URL:</b> <input type="text" name="page_url" size="35"></input><br/><small>URL Example: my_gallery_tut (<b>no special caracters or spaces!</b>)</small><br/><br/>
<textarea name="page_content" id="advanced"></textarea><br/>
<b>Status:</b> <select name="status">
<option value="active">Active</option>
<option value="inactive">Inactive</option>
</select><br/>
<center><input type="submit" value="Add Page"></input></center>
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
<br/>


';

//Add Editor to textarea  
  require("../gallery_includes/editorfooter.php");

?>