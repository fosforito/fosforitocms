<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Add Editor to textarea  
  require("../gallery_includes/editorheader.php");
  
//Load Language-Pack
  require("../gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_edit.php");

if(isset($_POST['update']))
{

  
$edit_id        = $_POST['update'];
$edit_title     = $_POST['edit_title'];
$edit_descr     = $_POST['edit_descr'];
$edit_klicks    = $_POST['edit_klicks'];
$edit_descr     = str_replace("'", "''", $edit_descr);

	  
	  $query = "UPDATE gal_pics SET pic_title = '$edit_title' WHERE pic_id = '$edit_id'";
      $result = mysql_query($query);
	  
	  $query = "UPDATE gal_pics SET pic_descr = '$edit_descr' WHERE pic_id = '$edit_id'";
      $result = mysql_query($query);
	  
	  $query = "UPDATE gal_pics SET pic_klicks = '$edit_klicks' WHERE pic_id = '$edit_id'";
      $result = mysql_query($query);
	  
	  
echo $_e['lg_edit_ready'];
   
}



if(isset($_GET['edit_id']))
{

$edit_id = $_GET['edit_id'];

      $query = "SELECT * FROM gal_pics WHERE pic_id = '$edit_id'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);

$_title = $obj->pic_title;
$_descr = $obj->pic_descr;
$_klicks = $obj->pic_klicks;



echo '
<form method="post" action="index.php?c=edit.php&edit_id='.$edit_id.'">
<input type="hidden" name="update" value="'.$edit_id.'"></input>
<table width="" border="0" align="center">
<tr>
<td align="right" valign="top"><b>'.$_e["lg_edit_title"].':</b></td>
<td align="left" valign="top"> 
<input type="text" name="edit_title" value="'.$_title.'"></input>
</td>
</tr>
<tr>
<td align="right" valign="top"><b>'.$_e["lg_edit_descr"].':</b></td>
<td align="left" valign="top">
<textarea id="simple" name="edit_descr" rows="5" cols="30">'.$_descr.'</textarea>
</td>
</tr>
<tr>
<td align="right" valign="top"><b>'.$_e["lg_edit_klicks"].':</b></td>
<td align="left" valign="top">
<input type="text" name="edit_klicks" value="'.$_klicks.'"></input>
</td>
</tr>
<tr>
<td colspan="2" align="center" valign="top">
<input type="submit" value="'.$_e["lg_edit_submit"].'"></input>
</td>
</tr>
</table>
</form>';


} else {
echo '<h3>No ID to edit selected!</h3>';
}

//Add Editor to textarea  
  require("../gallery_includes/editorfooter.php");
  
?>