<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

if(isset($_GET['delete'])){

   $deluser = $_GET['delete'];

      if(isset($_POST['delete_now'])){
         if($_POST['delete_now'] == "yes"){

               mysql_query("DELETE FROM gal_users WHERE user_name = '$deluser'");
               mysql_query("DELETE FROM gal_profiles WHERE profile_name = '$deluser'");
               echo '<div class="notice" align="center">Deleted...<br/></div><br/>';
			   
         } elseif($_POST['delete_now'] == "no"){
               
              echo '<div class="notice" align="center">Not deleted...<br/></div><br/>';
			  
         }

      } else {

           echo '<div class="notice" align="center"><br/>Are you sure, you want to delete this User Account?';
		   
		   echo '<br/>('.$deluser.')';

           echo '<table border="0"><tr><td><form action="index.php?c=gal_users.php&delete='.$deluser.'" method="post">
                 <input type="hidden" name="delete_now" value="yes"></input>
                 <input type="submit" value="Yes"></input>
                 </form>
				 </td><td>
                 <form action="index.php?c=gal_users.php&delete='.$deluser.'" method="post">
                 <input type="hidden" name="delete_now" value="no"></input>
                 <input type="submit" value="No"></input>
                 </form></td></tr></table></div><br/>';
      }
}


//Userlist
if(isset($_GET['view'])){

   $usertoview = $_GET['view'];

     $result = mysql_query("SELECT * FROM gal_users WHERE user_name = '$usertoview'");
     $obj = Mysql_fetch_object($result);  
	 
     $usertoviewuser = $obj->user_name;
     $usertoviewlevel = $obj->user_level;
     $zuserprotected = $obj->usrprotected;
	 
if($usertoviewlevel == 1){
   $usertoviewlevel = "Administrator";
} elseif($usertoviewlevel == 2){
   $usertoviewlevel = "Editor";
} elseif($usertoviewlevel == 3){
   $usertoviewlevel = "User";
} elseif($usertoviewlevel == 4){
   $usertoviewlevel = "Blocked";
} else {
   $usertoviewlevel = "No level?! Hmm...";
}

  echo '
     <table width="100%" align="center" border="0">
     <tr>
     <td colspan="2" align="right">
   
     <a href="index.php?c=gal_users.php&action=edit&zedit='.$usertoviewuser.'" title="Edit '.$usertoviewuser.'"><img src="../gallery_includes/images/edit2.png" width="20px" height="20px"></img></a>';
   
if($zuserprotected != 1){ 
   echo '&nbsp;&nbsp;
   <a href="index.php?c=gal_users.php&delete='.$usertoviewuser.'" title="Delete '.$usertoviewuser.'"><img src="../gallery_includes/images/delete2.png" width="15px" height="15px"></img></a>';
}

echo '</td>
   </tr>
   <tr>
   <td align="right" width="200px">Username:</td>
   <td align="left" width="200px">'.$usertoviewuser.'</td>
   </tr>
   <tr>
   <td align="right" width="200px">Userlevel:</td>
   <td align="left" width="200px">'.$usertoviewlevel.'</td>
   </tr></table>
   <br/>
   ';
	 
} else {


  
$sql = mysql_query("SELECT * FROM gal_users ORDER BY user_name ASC");

///////////////// PAG LOGIK //////////////////////////

$nr = mysql_num_rows($sql); // Get total of Num rows from the database query
if (isset($_GET['pn'])) { // Get pn from URL vars if it is present
    $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']); // filter everything but numbers for security
	
} else { // If the pn URL variable is not present force it to be value of page number 1
    $pn = 1;
} 

//This is where we set how many database items to show on each page 
$itemsPerPage = 30; 

// Get the value of the last page in the pagination result set
$lastPage = ceil($nr / $itemsPerPage);

// Be sure URL variable $pn(page number) is no lower than page 1 and no higher than $lastpage
if ($pn < 1) { // If it is less than 1
    $pn = 1; // force if to be 1
	
} else if ($pn > $lastPage) { // if it is greater than $lastpage
    $pn = $lastPage; // force it to be $lastpage's value
} 

// This creates the numbers to click in between the next and back buttons
// This section is explained well in the video that accompanies this script
$centerPages = "";
$sub1 = $pn - 1;
$sub2 = $pn - 2;
$add1 = $pn + 1;
$add2 = $pn + 2;
if ($pn == 1) {
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
} else if ($pn == $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
} else if ($pn > 2 && $pn < ($lastPage - 1)) {
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
} else if ($pn > 1 && $pn < $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=gal_users.php&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
}

// This line sets the "LIMIT" range... the 2 values we place to choose a range of rows from database in our query
$limit = 'LIMIT ' .($pn - 1) * $itemsPerPage .',' .$itemsPerPage; 

// Now we are going to run the same query as above but this time add $limit onto the end of the SQL syntax
// $sql2 is what we will use to fuel our while loop statement below
$sql2 = mysql_query("SELECT * FROM gal_users ORDER BY user_name ASC $limit"); 

$paginationDisplay = ""; // Initialize the pagination output variable
// This code runs only if the last page variable is ot equal to 1, if it is only 1 page we require no paginated links to display
if ($lastPage != "1"){

    // This shows the user what page they are on, and the total number of pages
    $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage. '&nbsp;  &nbsp;  &nbsp; ';
	
    // If we are not on page 1 we can place the Back button
    if ($pn != 1) {
        $previous = $pn - 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=gal_users.php&pn=' . $previous . '"> Back</a> ';
    } 
	
    // Lay in the clickable numbers display here between the Back and Next links
    $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
	
    // If we are not on the very last page we can place the Next button
    if ($pn != $lastPage) {
        $nextPage = $pn + 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=gal_users.php&pn=' . $nextPage . '"> Next</a> ';
    } 
}

// Build the Output Section Here
// $outputList = '';
$cnt = 0;
     
	 $siteurl = get_gal_conf('set_url');

echo '<table valign="middle" align="center" width="100%">
<tr>
<td>
<h4>Username</h4>
</td>
<td align="left">
<h4>Userlevel</h4>
</td>
<td align="right">
<h4>Edit/Details</h4>
</td>
</tr>';


while($row = @mysql_fetch_array($sql2)){ 

    $user = $row['user_name'];
    $level = $row["user_level"];
	
if($level == 1){
$level = "<font color='red'>Administrator</font>";
} elseif($level == 2){
$level = "<font color='blue'>Editor</font>";
} elseif($level == 3){
$level = "<font color='green'>User</font>";
} elseif($level == 4){
$level = "<font color='orange'>Blocked</font>";
} else {
$level = "<font color='black'>:O No level?!</font>";
}

// outputList
	
	echo "<tr><td width='300px' align='left'>
              ".$user."
			  </td>
			  <td width='100px' align='left'>
			  ".$level."
              </td>
			  <td width='200px' align='right'>
			  <a href='index.php?c=gal_users.php&view=$user' title='View \"$user\" Details'>View Details</a>
			  <a href='index.php?c=gal_users.php&action=edit&zedit=$user' title='Edit \"$user\"'><img src='../gallery_includes/images/edit2.png' width='20px' height='20px'></img></a>
			  </td>
			  </tr>";
	$cnt++;
    if($cnt >= get_gal_conf('pics_per_row'))
        {
          //echo "</tr></tr>";
          $cnt = 0;
        }	
	
    
} // close while loop
echo '</table><br/>';

if($nr != 0){
    echo '<div class="pagination" align="center">';
    echo $paginationDisplay;
    echo '</div>';
}else{
    echo 'o_O';
}

}

?>