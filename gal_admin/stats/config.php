<?PHP

//Other Settings
$db_prefix = 'gal_stats_'; // database prefix
$style = "dark"; // Counter Style "dark" or "light"
$show = "totally"; // Counter shows "totally"  or "last24h"  visitors
$size = "big"; // Size of the counter "small" or "big"

$reload=3*60*60; // Reload lock in seconds (3 * 60 * 60 => 3 hours)
$online=3*60; // online time in seconds (3 * 60 => 3 minutes)
$oldentries=30; // delete Visitor infos after x days (30 => 30 days)
//End of Settings

?>