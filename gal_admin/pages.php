<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

if ($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass AND $user_level == 1) {

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else {
        $action = 'all';
    }

    if ($action == 'new') {
        echo '<span class="admin_pad"><a href="index.php?c=pages.php&action=all">All Pages</a></span>
             <span class="admin_pad_on">New Page</span>
             <br/><br/>';
    } elseif ($action == 'edit') {
        echo '<span class="admin_pad"><a href="index.php?c=pages.php&action=all">All Pages</a></a></span>
             <span class="admin_pad"><a href="index.php?c=pages.php&action=new">New Page</a></span>
             <span class="admin_pad_on">Edit Page</span>
             <br/><br/>';
    } else {
        echo '<span class="admin_pad_on">All Pages</span>
             <span class="admin_pad"><a href="index.php?c=pages.php&action=new">New Page</a></span>
             <br/><br/>';
    }

    if ($action == 'new') {
        //Load
        require('./sources/new_page.php');
    } elseif ($action == 'edit') {
        //Load
        require('./sources/edit_page.php');
    } else {
        //Load
        require('./sources/pages.php');
    }
}
?>
