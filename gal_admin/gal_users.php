<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

if($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass AND $user_level == 1){ 
    
    if(isset($_GET['action'])){
        $action = $_GET['action'];
    } else {
        $action = 'all';
    }
    
    if($action == 'add'){
        
        echo '<span class="admin_pad"><a href="index.php?c=gal_users.php">All Users</a></span>
        <span class="admin_pad_on">Add User</span>
        <br/><br/>';
        
        require('./sources/adduser.php');
    
    } elseif($action == 'edit') {
        
        echo '<span class="admin_pad"><a href="index.php?c=gal_users.php">All Users</a></span>
        <span class="admin_pad"><a href="index.php?c=gal_users.php&action=add">Add User</a></span>
        <span class="admin_pad_on">Edit User</span>
        <br/><br/>';
        
        require('./sources/edit_user.php');
        
    } else {
        
        if(isset($_GET['view'])){
            
             echo '
             <span class="admin_pad"><a href="index.php?c=gal_users.php">All Users</a></span>
             <span class="admin_pad"><a href="index.php?c=gal_users.php&action=add">Add User</a></span>
             <span class="admin_pad_on">View User</span>
             <br/><br/>';
             
        } else {
            
             echo '
             <span class="admin_pad_on">All Users</span>
             <span class="admin_pad"><a href="index.php?c=gal_users.php&action=add">Add User</a></span>
             <br/><br/>';
             
        }

        //Load
        require('./sources/gal_users.php');
    
    }
}

?>
