<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

$rss = simplexml_load_file('http://localhost/fcms/gal_rss.xml'); 

if ($rss===null || !is_object($rss))
    die('Failed to load xml file.');
if (!is_object($rss->channel))
    die('Channel is not an object!');

foreach ($rss->channel->item as $item){
    if (is_object($item)) {
        $namespaces = $rss->getNameSpaces(true);
        $dc = $rss->children($namespaces['dc']);

        echo $item->pubDate . "<br>";
        echo $item->title . "<br>";
        echo $item->description . "<br>";
        echo "<a href='" . $item->link . "'>&gt;&gt;</a><br><br>";
    }
}




//for($i = 0; $i < 3; $i++){
//     $title = $xml->channel->item.$i->title;
//     $link = $xml->channel->item.$i->link;
//     $description = $xml->channel->item.$i->description;
//     $pubDate = $xml->channel->item.$i->pubDate;
//
//     $html .= "<a href='$link'><h3>$title</h3></a>";
//     $html .= "$description";
//     $html .= "<br />$pubDate<hr />";
//}
//
//echo $html;

?>
