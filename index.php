<?php
session_start();
 
/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Index Page of FosforitoCMS to load all public Contents
//Last modification: 17.07.2013 by Jens Leon Wagner

//Load Configuration...
  require("config_gallery.php");
  require("gallery_includes/get_gal_conf.php");

//Get User Session Vars and verificate them
  $user_name = @$_SESSION['user_name'];
  $user_pass = @$_SESSION['user_pass'];
  $user_level = @$_SESSION['user_level'];
  require('gallery_includes/user_ver.php');   


//Load Page Content
  if(isset($_GET['c'])){
      $c = $_GET['c'];  
      if($c != ""){
          //Load Page Content with "c" variable from URL
          require('gallery_includes/themes/gal_default/gal/'.$c.'.php');
      }
  } else {  
      //Load Default Homepage-Content
      require('gallery_includes/themes/gal_default/gal/index.php');
  }
  
//Close MySQL Connection
  mysql_close();

?>
