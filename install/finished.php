<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

  echo '<html>
          <head>
            <title>
              FosforitoCMS | Installation Step 3
            </title>';
	
//Load CSS
  echo '<link rel="stylesheet" type="text/css" href="../gal_admin/sources/styles/gal_default.css" />';
  echo '  </head>
          <body>';

     ////////////////////////////////////////////////////////////
    ///////////////////////       Create Tables         ////////
                  ////////    in selected Database     ///////////////////////
                 ////////////////////////////////////////////////////////////

//Create DB Error Function
function own_db_error(){
echo "
<table width='400px' border='0' align='center'>
  <tr><td>
    <div align='center' class='complete_table_error'>
      <h2>An Error ocurred:</h2>
      ".mysql_error()."
      <br/><br/>
    </div>
  </td></tr>
</table>";
}

   $post_server = $_POST['server'];
   $post_database = $_POST['database'];
   $post_user = $_POST['user'];
   $post_password = $_POST['password'];
   $post_admin_user = $_POST['admin_user'];
   $post_admin_email = $_POST['admin_email'];
   $post_admin_pass = md5($_POST['admin_pass']);
   $post_site_lang = $_POST['site_lang'];
   $post_site_url = $_POST['site_url'];
   $post_site_name = $_POST['site_name'];
   $post_absolute_path = $_POST['absolute_path'];


   //Connecting to Database - or die
     mysql_connect("$post_server", "$post_user", "$post_password") or die(own_db_error());
     mysql_select_db("$post_database") or die(own_db_error());



##### Picture Table (Gallery) #####

mysql_query("CREATE TABLE gal_pics (
	  pic_id INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
	  pic_dat CHAR(100) NOT NULL,
	  pic_date INT(11) NOT NULL,
	  pic_klicks INT(11) NOT NULL,
	  pic_title CHAR (100) NOT NULL,
	  pic_descr TEXT(20000) NOT NULL
	)") Or die(own_db_error());

##### Users Table #####
	
mysql_query("CREATE TABLE gal_users (
	  user_name CHAR(50) NOT NULL PRIMARY KEY,
          user_email CHAR(150) NOT NULL,
	  user_pass CHAR(100) NOT NULL,
	  user_level CHAR(30) NOT NULL,
          accode CHAR(30) NOT NULL,
          status CHAR(20) NOT NULL,
	  usrprotected INT(2)NOT NULL
	)") Or die(own_db_error());
	
$query = "INSERT INTO gal_users (user_name, user_email, user_pass, user_level, status, usrprotected) VALUES ('$post_admin_user', '$post_admin_email', '$post_admin_pass', '1', 'active', '1')";
mysql_query($query);

##### User Profiles Table #####

mysql_query("CREATE TABLE gal_profiles (
	  profile_name CHAR(50) NOT NULL PRIMARY KEY,
	  profile_avrtr CHAR(150) NOT NULL,
	  profile_email CHAR(100) NOT NULL,
	  profile_bio LONGTEXT NOT NULL
	)") Or die(own_db_error());

$query = "INSERT INTO gal_profiles (profile_name, profile_avrtr, profile_email) VALUES ('$post_admin_user', 'default_gal_avrtr.jpg', '$post_admin_email')";
mysql_query($query);

##### Pages Table #####

mysql_query("CREATE TABLE gal_pages (
	  page_title CHAR(200) NOT NULL,
	  page_url CHAR(200) PRIMARY KEY NOT NULL,
	  page_date INT(15) NOT NULL,
	  page_lupdate INT(15) NOT NULL,
	  page_content LONGTEXT NOT NULL,
	  page_status CHAR(30) NOT NULL	  
	)") Or die(own_db_error());

$time = time();

/* Default Page: Homepage */

$query = "INSERT INTO gal_pages (page_title, page_url, page_date, page_lupdate, page_content, page_status) VALUES ('Home', 'home', '$time', '$time', '
<table style=\"line-height: 1.6em; margin-left: auto; margin-right: auto; width: 80%;\" border=\"0\">
  <tbody>
    <tr>
      <td>
        <div>
          <span style=\"font-family:comic sans ms,cursive\"><small>Welcome!</small></span> 
          <hr />
        <div style=\"padding-left: 50px; padding-top: 0px; padding-bottom: 5px; padding-right: 30px;\">
      <h3>Welcome to FosforitoCMS!<img style=\"float: right; height: 100px; width: 100px;\" src=\"".$post_site_url."/gallery_includes/images/gallery.png\" alt=\"Image\" /></h3>
   <ul>
      <li>Costumize the Content of your Gallery<br /> Index in the Admin Center.</li>
      <li>Gestalte den Inhalt deiner Gallerie-Homepage<br /> im Admin-Bereich.</li>
      <li>Personliz&aacute; el Contenido de la P&aacute;gina<br /> principal de tu Galer&iacute;a en el Centro de Administraci&oacute;n.</li>
   </ul>
        </div>
          <hr />
        </div>
      </td>
    </tr>
  </tbody>
</table>
', 'active')";
mysql_query($query);

##### Blog Posts Table #####

mysql_query("CREATE TABLE gal_blog (
	  blog_title CHAR(100) NOT NULL,
	  blog_url CHAR(150) PRIMARY KEY NOT NULL,
	  blog_cat CHAR(150) NOT NULL,
	  blog_date INT(50) NOT NULL,
	  blog_lupdate INT(50) NOT NULL,
	  blog_dat LONGTEXT NOT NULL,
	  blog_status CHAR(20) NOT NULL,
          blog_author CHAR(100) NOT NULL
	)") Or die(own_db_error());

##### Menu Item Table (Public) #####

mysql_query("CREATE TABLE gal_nav (
          id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
          nav_sub_id INT(11) NOT NULL,
	  nav_id INT(11) NOT NULL,
	  nav_title CHAR(150) NOT NULL,
	  nav_link CHAR(150) NOT NULL,
	  nav_tab INT(2) NOT NULL
	)") Or die(own_db_error());

$query = "INSERT INTO gal_nav (nav_title, nav_link, nav_tab, nav_id) VALUES ('Home', 'index.php', '1', '1')";
mysql_query($query);

$query = "INSERT INTO gal_nav (nav_title, nav_link, nav_tab, nav_id) VALUES ('Blog', 'index.php?c=blog', '1', '2')";
mysql_query($query);

$query = "INSERT INTO gal_nav (nav_title, nav_link, nav_tab, nav_id) VALUES ('Gallery', 'index.php?c=showgalerie', '1', '3')";
mysql_query($query);

$query = "INSERT INTO gal_nav (nav_title, nav_link, nav_tab, nav_id) VALUES ('Members', 'index.php', '1', '4')";
mysql_query($query);

     $query = "INSERT INTO gal_nav (nav_title, nav_link, nav_tab, nav_id, nav_sub_id) VALUES ('Registration', 'index.php?c=gal_register', '2', '1', '4')";
     mysql_query($query);

     $query = "INSERT INTO gal_nav (nav_title, nav_link, nav_tab, nav_id, nav_sub_id) VALUES ('Activation', 'index.php?c=gal_activate', '2', '2', '4')";
     mysql_query($query);

##### Menu Item Table for AdminCenter #####
	
mysql_query("CREATE TABLE gal_admin (
	  cont_id INT(11) AUTO_INCREMENT PRIMARY KEY NOT NULL,
	  cont_ico CHAR(20) NOT NULL,
	  cont_date CHAR(100) NOT NULL,
	  cont_nav CHAR(200) NOT NULL,
	  cont_dat CHAR(200) NOT NULL,
	  sub_nav CHAR(200) NOT NULL,
	  sub_dat CHAR(200) NOT NULL,
	  sub_cont_nav CHAR(200) NOT NULL,
	  sub_cont_dat CHAR(200) NOT NULL,
	  tab INT(2) NOT NULL
	)") Or die(own_db_error());
	
$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe000', 'Dashboard', 'admin_index.php', '1')";
mysql_query($query);

$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe020;', 'Statistics', 'gal_stats.php', '1')";
mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Statistics', 'gal_stats.php', 'One View', 'gal_stats.php', '2')";
     mysql_query($query);
	 
     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Statistics', 'gal_stats.php', 'Visitors', 'gal_stats.php&p=visitors', '2')";
     mysql_query($query);
	 
     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Statistics', 'gal_stats.php', 'History', 'gal_stats.php&p=history', '2')";
     mysql_query($query);

$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe038;', 'Users', 'gal_users.php', '1')";
mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Users', 'gal_users.php', 'All Users', 'gal_users.php', '2')";
     mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Users', 'gal_users.php', 'Add User', 'gal_users.php&action=add', '2')";
     mysql_query($query);

$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe001;', 'Blog', 'gal_blog.php', '1')";
mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Blog', 'gal_blog.php', 'All Posts', 'gal_blog.php', '2')";
     mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Blog', 'gal_blog.php', 'New Post', 'gal_blog.php&action=new', '2')";
     mysql_query($query);

$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe003;', 'Gallery', 'galimages.php', '1')";
mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Gallery', 'galimages.php', 'All Pictures', 'galimages.php', '2')";
     mysql_query($query);
	 
	 $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Gallery', 'galimages.php', 'Add Picture', 'galimages.php&action=add', '2')";
     mysql_query($query);

$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe006;', 'Pages', 'pages.php', '1')";
mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Pages', 'pages.php', 'All Pages', 'pages.php', '2')";
     mysql_query($query);
	 
	 $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Pages', 'pages.php', 'New Page', 'pages.php&action=new', '2')";
     mysql_query($query);
	 
	 $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Pages', 'pages.php', 'Edit Homepage', 'pages.php&action=edit&edit=home', '2')";
     mysql_query($query);
     
$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe009;', 'Menus', 'menu.php', '1')";
mysql_query($query);

$query = "INSERT INTO gal_admin (cont_ico, cont_nav, cont_dat, tab) VALUES ('&#xe010;', 'Settings', 'settings.php', '1')";
mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Settings', 'settings.php', 'General Settings', 'settings.php&tab=1', '2')";
     mysql_query($query);
     
     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Settings', 'settings.php', 'Permissions', 'settings.php&tab=4', '2')";
     mysql_query($query);

     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Settings', 'settings.php', 'Gallery Settings', 'settings.php&tab=2', '2')";
     mysql_query($query);
     
     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Settings', 'settings.php', 'Styles', 'settings.php&tab=3', '2')";
     mysql_query($query);
     
     $query = "INSERT INTO gal_admin (sub_nav, sub_dat, sub_cont_nav, sub_cont_dat, tab) VALUES ('Settings', 'settings.php', 'My Profile', 'editprofile.php', '2')";
     mysql_query($query);
     

     
##### Comment Table (Blog) #####
     
mysql_query("CREATE TABLE gal_blog_coms (
          com_id INT(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
	  com_url CHAR(100) NOT NULL,
	  com_date CHAR(100) NOT NULL,
          com_author CHAR(100) NOT NULL,
          com_email CHAR(100) NOT NULL,
          com_web CHAR(100) NOT NULL,
          com_data LONGTEXT NOT NULL,
          com_reg INT(2) NOT NULL
	)") Or die(own_db_error());

##### Configuration Table #####

mysql_query("CREATE TABLE gal_conf (
	  conf_name CHAR(100) PRIMARY KEY NOT NULL,
	  conf_value CHAR(100) NOT NULL
	)") Or die(own_db_error());

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_admin_bar', 'no')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('allow_regs', '0')"; // Default: 0 or 1
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_menu_order', 'ASC')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('pics_per_row', '5')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('pics_per_page', '30')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('gal_maxfilesize', '2000000')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_order_by', 'DESC')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_image_maxheight', '550')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_image_maxwidth', '600')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_site_language', '$post_site_lang')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_url', '$post_site_url')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_site_name', '$post_site_name')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_site_slogan', 'Creating Websites is easy...')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('site_width', '950px')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('file_path', 'media/gal_pics/')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('set_stylesheet', 'gal_default.css')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('show_klicks', 'no')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('show_dats', 'yes')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('gal_version', 'Version 1.0')";
mysql_query($query);

$query = "INSERT INTO gal_conf (conf_name, conf_value) VALUES ('absolute_path', '$post_absolute_path')";
mysql_query($query);




##### Create Configuration File #####

$text = '<?php

#################  MyGallery - Configuration File  ###################

  ###### Section: MySQL-Database Details ######
  //Database-server, generally it is localhost
  $server    = "'.$post_server.'"; 
  
  // Database Name, must already exist
  $database  = "'.$post_database.'"; 
  
  //The username for the Database
  $user      = "'.$post_user.'"; 
  
  //The Password for the Database
  $password  = "'.$post_password.'"; 	
  
  //Root Directory - Absolute Path to your Website
  $absolute_path = "'.$post_absolute_path.'";
  
###################################################################### 

//Connecting to Database
  @mysql_connect("$server", "$user", "$password") or die(\'<h1>Cannot connect to MySQL Server...</h1>\');
  @mysql_select_db("$database") or die(\'<h1>Cannot connect to Database...</h1>\'); 
  
?>';
$file = "../config_gallery.php";
$create = fopen($file, 'w');
fwrite($create, $text);

fclose($create);

###############################################################################################



// Stats Table Days
mysql_query("CREATE TABLE `gal_stats_Day` (
  	`id` int(11) NOT NULL auto_increment,
	`day` varchar(10) NOT NULL default '',
	`user` int(10) NOT NULL default '0',
	`view` int(10) NOT NULL default '0',
	PRIMARY KEY  (`id`)
	)");


// Stats Table IPs
mysql_query("CREATE TABLE `gal_stats_IPs` (
	`id` int(11) NOT NULL auto_increment,
	`ip` varchar(15) NOT NULL default '',
	`time` int(20) NOT NULL default '0',
	`online` int(20) NOT NULL default '0',
	PRIMARY KEY  (`id`)
	)");


// Stats Table Pages
mysql_query("CREATE TABLE `gal_stats_Page` (
	`id` int(11) NOT NULL auto_increment,
	`day` varchar(10) NOT NULL default '',
	`page` varchar(255) NOT NULL default '',
	`view` int(10) NOT NULL default '0',
	PRIMARY KEY  (`id`)
	)");


// Stats Table Refferer
mysql_query("CREATE TABLE `gal_stats_Referer` (
	`id` int(11) NOT NULL auto_increment,
	`day` varchar(10) NOT NULL default '',
	`referer` varchar(255) NOT NULL default '',
	`view` int(10) NOT NULL default '0',
	PRIMARY KEY  (`id`)
	)");


// Stats Table Keywords
mysql_query("CREATE TABLE `gal_stats_Keyword` (
	`id` int(11) NOT NULL auto_increment,
	`day` varchar(10) NOT NULL default '',
	`keyword` varchar(255) NOT NULL default '',
	`view` int(10) NOT NULL default '0',
	PRIMARY KEY  (`id`)
	)");


// Stats Table Languages
mysql_query("CREATE TABLE `gal_stats_Language` (
	`id` int(11) NOT NULL auto_increment,
	`day` varchar(10) NOT NULL default '',
	`language` varchar(2) NOT NULL default '',
	`view` int(10) NOT NULL default '0',
	PRIMARY KEY  (`id`)
	)");


//Load Configuration...
  require("../gallery_includes/get_gal_conf.php");


//Create Output...
echo'
<table width="500px" border="0" align="center">
  <tr>
    <td>
      <div align="center" class="complete_table">

        <h1 style="margin-bottom:0; color: #333333">FosforitoCMS</h1>
        <h3 style="margin-top:0; color: #555555">Installation 3/3</h3>

        <h2>Your new Website is ready!</h2>
        
        Now, please delete the "install"-directory
        <br/>
        and then you can start to upload images to
        <br/>
        your Gallery, create Pages,
        <br/>
        publish articles in your
        <br/>
        Blog and more!
        <br/>
        <br/>
        
        <hr width="80%">
        
        <a href="../index.php">
           <b>Home</b>
        </a>
        
      </div>
    </td>
  </tr>
</table>';

//Close MySQL Connection
  mysql_close();

echo '
  </body>
</html>';

?>
