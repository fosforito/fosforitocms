<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

if(isset($_GET['step'])){
if($_GET['step'] == 2){
    
$var1 = $_SERVER["DOCUMENT_ROOT"];
$tempstore = explode('/',$_SERVER['SCRIPT_NAME']);
$path = '';
$total = '';
foreach($tempstore as $pi){
	if($pi == 'install'){
		$path = $total;
	} elseif($pi != '') {
	    $total.= '/'.$pi;
	}
}
$var1 .= $path;

$var2 = 'http://';
$var2 .= $_SERVER['HTTP_HOST'];
$var2 .= $path;

echo'<html>
<head>
<link rel="stylesheet" type="text/css" href="../gal_admin/sources/styles/gal_default.css" />
</head>
<body>

<table width="500px" border="0" align="center">
<tr>
<td>

<div align="center" class="complete_table">

<h1 style="margin-bottom:0; color: #333333">FosforitoCMS</h1>
<h3 style="margin-top:0; color: #555555">Installation 2/3</h3>

<form action="finished.php" method="post">

<table width="100%" height="100%" cellpadding="3" border="0">

<tr><td colspan="2" align="center"><br/><b>Database Connection Information:</b></td></tr>
<tr><td align="right">Server:</td><td align="left"><input type="text" name="server" value="localhost"></input>
<a title="Try to use localhost if you don\'t know what is that. Or contact your Hosting Provider.">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td align="right">Database (MySQL):</td><td align="left"><input type="text" name="database"></input>
<a title="The Database must exist. It is to store tha Data of your Gallery.">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td align="right">User:</td><td align="left"><input type="text" name="user"></input>
<a title="The Username to access the Database.">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td align="right">Password:</td><td align="left"><input type="text" name="password"></input>
<a title="The Password to access the Database">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td colspan="2" align="center"><br/><b>Details for Admin-Account:</b></td></tr>
<tr><td align="right">Username:</td><td align="left"><input type="text" name="admin_user"></input>
<a title="Set a Username to upload Images to your Gallery.">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td align="right">Email:</td><td align="left"><input type="text" name="admin_email"></input>
<a title="Your Admin Email for Notifications, etc...">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td align="right">Password:</td><td align="left"><input type="text" name="admin_pass"></input>
<a title="Set a Password to upload Images to your Gallery">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td colspan="2" align="center"><br/><b>General Gallery Settings:</b></td></tr>
<tr><td align="right">Gallery Name:</td><td align="left"><input type="text" name="site_name"></input>
<a title="How would you like to name your new Gallery?">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>

<tr><td align="right">Site Language:</td><td align="left">
<select name="site_lang">
<option value="en">English</option>
<option value="de">German</option>
<option value="es">Spanish</option>
</select>
<a title="Set here the Language you want for your Gallery">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>';


echo '<tr><td align="right">Absolute Path:</td><td align="left"><input type="text" name="absolute_path" value="'.$var1.'"></input>
<a title="The absolute Path to your Webspace">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>';


echo '<tr><td align="right">Gallery URL:</td><td align="left"><input type="text" name="site_url" value="'.$var2.'"></input>
<a title="The URL to your Gallery (without \'/\' at the end)">
<img src="../gallery_includes/images/ask1.jpg"></img></a></td></tr>


</table>

<input type="submit" value="Install"></input>
</form>


</div>

</td>
</tr>
</table>
<center>
<small>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">FosforitoCMS</a><br/>Version 1.0</small>
</center>
</body>
</html>';

}} else {

echo'<html>
<head>
<link rel="stylesheet" type="text/css" href="../gal_admin/sources/styles/gal_default.css" />
<style type="text/css">
<!--
#st1 td{
     border: 1px solid #999999;
     padding: 2px 5px;
     
}
-->
</style>
</head>
<body>

<table width="500px" border="0" align="center">
<tr>
<td>

<div align="center" class="complete_table">

<h1 style="margin-bottom:0; color: #333333">FosforitoCMS</h1>
<h3 style="margin-top:0; color: #555555">Installation 1/3</h3>';

echo '
<p><b>Welcome to the first Release of FosforitoCMS!</b></p>
<p>This simple and lightweight CMS is designed<br/>for people with small Proyects like a Personal<br/>Homepage or Image Gallery.</p>
';


//Check if correct permissions for writing are set...
    echo '<table id="st1"><tr>';
    echo '<tr><td align="left" width="200x"><b>File/Directory</b></td><td align="right" width="100px"><b>Status</b></td></tr>';
    
echo '<td align="left">/</td>';
if(is_writable('../')){
    echo '<td align="right"><span style="color:green;">Is writable<br/></span></td>';
}else{
    echo '<td align="right"><span style="color:red;">Not writable<br/></span></td>';
}

echo '</tr><tr>';

echo '<td align="left">/gal_admin/stats/</td>';
if(is_writable('../gal_admin/stats/')){
    echo '<td align="right"><span style="color:green;">Is writable<br/></span></td>';
}else{
    echo '<td align="right"><span style="color:red;">Not writable<br/></span></td>';
}

echo '</tr><tr>';

echo '<td align="left">/media/gal_pics/</td>';
if(is_writable('../media/gal_pics/')){
    echo '<td align="right"><span style="color:green;">Is writable<br/></span></td>';
}else{
    echo '<td align="right"><span style="color:red;">Not writable<br/></span></td>';
}

echo '</tr><tr>';

echo '<td align="left">/media/gal_pics/avrtrs/</td>';
if(is_writable('../media/gal_pics/avrtrs/')){
    echo '<td align="right"><span style="color:green;">Is writable<br/></span></td>';
}else{
    echo '<td align="right"><span style="color:red;">Not writable<br/></span></td>';
}

echo '</tr></table>';
//End Permission check


echo '<br/><b>If everything is green,<br/><a href="index.php?step=2">Go to Step 2 &gt;&gt;</a></b><br/><br/>';

echo '<!--LOAD FOOTER-->
</div>
</td>
</tr>
</table>
<center>
<small>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">FosforitoCMS</a><br/>Version 1.0</small>
</center>
</body>
</html>';

}

?>