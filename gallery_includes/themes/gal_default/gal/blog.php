 <?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
  <head>
    <title>
      '.get_gal_conf('set_site_name').' | Blog
    </title>';
  
//Load CSS
echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/fonts/icomoon/style.css"/>';
echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/themes/gal_default/gal_default_main.css" />';

echo '</head><body>';

echo '<div id="container">
      <div id="page">
      <div id="header">';

//Load Header
include("gallery_includes/themes/gal_default/header.php"); 

echo '</div>
    <div id="menuhorizontal">';
require 'gallery_includes/themes/gal_default/nav.php';
 echo '</div><div style="clear: both;"> </div> 
     <div id="wrapper">
<div id="content">';


if(isset($_GET['article'])){
$url = $_GET['article'];

//Load Page content

      $query = "SELECT * FROM gal_blog WHERE blog_url = '$url'";
      $result = mysql_query($query);
      $obj = mysql_fetch_object($result);
	  
$showpage = @$obj->blog_dat;
$entrytitle = @$obj->blog_title;
$lastupdate = @$obj->blog_lupdate;
$date = @$obj->blog_date;
$lastupdate = date('Y-m-d H:i:s', $lastupdate);
$date = date('Y-m-d H:i:s', $date);
$author = @$obj->blog_author;
$statusss = @$obj->blog_status;

      $query2 = "SELECT * FROM gal_profiles WHERE profile_name = '$author'";
      $result2 = mysql_query($query2);
      $obj2 = mysql_fetch_object($result2);

$authoravrtr = @$obj2->profile_avrtr;
if($statusss == "active"){	  
echo "<table align='left' border='0' width='100%' class='entry'>
	<tr>
	<td width='80px' height='100px' class='avrtr'>
        <img src='".get_gal_conf('file_path')."avrtrs/".$authoravrtr."' width='50px' height='50px' class='avrtrimg'></img><br/><i>By ".$author."</i>
        </td>
	<td><h1 style='margin-left:10px; margin-bottom:10px;'>".$entrytitle."</h1>
        <hr style='margin-bottom:5px;'/>
        <small style='margin-left:10px;'><b>Published on: ".$date."</b></small>
        <hr style='margin-top:5px;'/></td>";
	
 if($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass AND $user_level == 1){ 	
echo"<td align='right' valign='top' width='22px'><a href='".get_gal_conf('set_url')."/gal_admin/index.php?c=gal_blog.php&action=new&ed=1&edit=".$url."' title='Edit \"".$entrytitle."\"'><img src='".get_gal_conf('set_url')."/gallery_includes/images/edit2.png' width='20px' height='20px'></img></a></td>";
} else {
echo "<td align='right' valign='top' width='1px'></td>";
}	
	echo "</tr>
    <tr>
    <td colspan='3'><table width='100%' border='0'><tr><td>".$showpage."</td></tr></table><br/></td>
	</tr>
	</table>
	<hr style='margin:0px;'/><small style='margin-left:10px;'><b>Last updated on: ".$lastupdate."</b></small>
    ";
        
        require('gallery_includes/themes/gal_default/gal/new_comment.php');
        
}else{
    echo '<center><h3 style="color:red;">ERROR: Article does not exist<br/>or is inactive!</h3><br/><br/></center>';
}

} else {


$sql = mysql_query("SELECT * FROM gal_blog ORDER BY blog_date DESC");

///////////////// PAG LOGIK //////////////////////////

$nr = mysql_num_rows($sql); // Get total of Num rows from the database query
if (isset($_GET['pn'])) { // Get pn from URL vars if it is present
    $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']); // filter everything but numbers for security
} else { // If the pn URL variable is not present force it to be value of page number 1
    $pn = 1;
} 

//This is where we set how many database items to show on each page 
$itemsPerPage = 10; 

// Get the value of the last page in the pagination result set
$lastPage = ceil($nr / $itemsPerPage);

// Be sure URL variable $pn(page number) is no lower than page 1 and no higher than $lastpage
if ($pn < 1) { // If it is less than 1
    $pn = 1; // force if to be 1
	
} else if ($pn > $lastPage) { // if it is greater than $lastpage
    $pn = $lastPage; // force it to be $lastpage's value
} 

// This creates the numbers to click in between the next and back buttons
$centerPages = "";
$sub1 = $pn - 1;
$sub2 = $pn - 2;
$add1 = $pn + 1;
$add2 = $pn + 2;
if ($pn == 1) {
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
} else if ($pn == $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
} else if ($pn > 2 && $pn < ($lastPage - 1)) {
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
} else if ($pn > 1 && $pn < $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
}

// This line sets the "LIMIT" range... the 2 values we place to choose a range of rows from database in our query
$limit = 'LIMIT ' .($pn - 1) * $itemsPerPage .',' .$itemsPerPage; 

// Now we are going to run the same query as above but this time add $limit onto the end of the SQL syntax
// $sql2 is what we will use to fuel our while loop statement below
$sql2 = mysql_query("SELECT * FROM gal_blog ORDER BY blog_date DESC $limit"); 

$paginationDisplay = ""; // Initialize the pagination output variable
// This code runs only if the last page variable is ot equal to 1, if it is only 1 page we require no paginated links to display
if ($lastPage != "1"){

    // This shows the user what page they are on, and the total number of pages
    $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage. '&nbsp;  &nbsp;  &nbsp; ';
	
    // If we are not on page 1 we can place the Back button
    if ($pn != 1) {
        $previous = $pn - 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=blog&pn=' . $previous . '"> Back</a> ';
    } 
	
    // Lay in the clickable numbers display here between the Back and Next links
    $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
	
    // If we are not on the very last page we can place the Next button
    if ($pn != $lastPage) {
        $nextPage = $pn + 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=blog&pn=' . $nextPage . '"> Next</a> ';
    } 
}

// Build the Output Section Here
// $outputList = '';
$cnt = 0;

     $siteurl = get_gal_conf('set_url');

echo '<table align="center" border="0" width="100%"><tr><td>';
while($row = @mysql_fetch_array($sql2)){ 

    $title = $row['blog_title'];
    $url = $row["blog_url"];
	$date = $row['blog_date'];
	$lupdate = $row['blog_lupdate'];
        $status = $row["blog_status"];
	$content = $row['blog_dat'];
	$date = date('Y-m-d H:i:s', $date);
        $author = $row['blog_author'];

      $query2 = "SELECT * FROM gal_profiles WHERE profile_name = '$author'";
      $result2 = mysql_query($query2);
      $obj2 = mysql_fetch_object($result2);

$authoravrtr = $obj2->profile_avrtr;

// outputList
if($status == 'active'){	
echo "<table align='left' border='0' width='100%' class='entry'>
        <tr>
	    <td width='80px' height='100px' class='avrtr' valign='top'>
                <img src='".get_gal_conf('file_path')."avrtrs/".$authoravrtr."' width='50px' height='50px' class='avrtrimg'></img><br/><i>By ".$author."</i>
            </td>
	<td><a href='index.php?c=blog&article=$url'>
                <h1 style='margin-left:10px; margin-bottom:10px;'>".$title."</h1>
            </a>
            <hr style='margin-bottom:5px;'/>
                <small style='margin-left:10px;'><b>Published on: ".$date."</b></small>";

echo "<hr style='margin-top:5px;'/>
        </td>";
	
	echo "</tr>
    <tr>
    <td colspan='2'>".$content."<br/></td>
	</tr>
	</table><div><hr style='margin:0px'/></div>
    ";
	$cnt++;
	
} else{
    $cnt++;
}	
    
} // close while loop

echo '</td></tr></table><br/><center>';

if($nr != 0){
    
     echo $paginationDisplay;

}else{
    
     echo 'There are currently no Entrys in the Blog...';
    
}
	  
}

   echo '</center></div>';
       
   require 'gallery_includes/themes/gal_default/sidebar.php';

echo '</div>
<div id="footer">';
   
   //Load Footer
require("gallery_includes/themes/gal_default/footer.php");

echo '</div>
</div></div>
<small><center>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">MyGallery</a><br/>'.get_gal_conf('gal_version').'</center></small>
  </body>
</html>';


?>