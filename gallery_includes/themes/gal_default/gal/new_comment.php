<?php

/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Add Editor to textarea  
  require("./gallery_includes/editorheader.php");
  
$geart = $_GET['article']; //Get article URL to view comments and to comment
$commenterror = 9; //Means no Errors...

echo '<div class="comment_box">
      <h2 style="margin-bottom:5px;">Comments</h2>';

if(isset($_GET['com'])){
    if($_GET['com'] == 'new'){
        
        @$submitted_name = $_POST['comment_name'];
        @$submitted_text = $_POST['comment_text'];
         $submitted_date = time();
        @$com_reg = $_POST['reg'];
        
        if($submitted_name != '' AND $submitted_text != ''){
            $comquery = "INSERT INTO gal_blog_coms (com_url, com_date, com_data, com_author, com_email, com_web, com_reg) VALUES ('$geart', '$submitted_date', '$submitted_text', '$submitted_name', 'Email', 'Homepage', '$com_reg')" or die(mysql_error());
            mysql_query($comquery);
            //Display Thanks Message for the Comment
            $commenterror = 0; 
        } else {
            //Display Error Message - Failed to submit Comment
            $commenterror = 1;
        }
        
    }
}

$sql = mysql_query("SELECT * FROM gal_blog_coms WHERE com_url = '$geart' ORDER BY com_date ASC");

///////////////// PAG LOGIK //////////////////////////

$nr = mysql_num_rows($sql); // Get total of Num rows from the database query
if (isset($_GET['pn'])) { // Get pn from URL vars if it is present
    $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']); // filter everything but numbers for security
	
} else { // If the pn URL variable is not present force it to be value of page number 1
    $pn = 1;
} 

//This is where we set how many database items to show on each page 
$itemsPerPage = 50; 

// Get the value of the last page in the pagination result set
$lastPage = ceil($nr / $itemsPerPage);

// Be sure URL variable $pn(page number) is no lower than page 1 and no higher than $lastpage
if ($pn < 1) { // If it is less than 1
    $pn = 1; // force if to be 1
	
} else if ($pn > $lastPage) { // if it is greater than $lastpage
    $pn = $lastPage; // force it to be $lastpage's value
} 

// This creates the numbers to click in between the next and back buttons
// This section is explained well in the video that accompanies this script
$centerPages = "";
$sub1 = $pn - 1;
$sub2 = $pn - 2;
$add1 = $pn + 1;
$add2 = $pn + 2;
if ($pn == 1) {
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
} else if ($pn == $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
} else if ($pn > 2 && $pn < ($lastPage - 1)) {
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
} else if ($pn > 1 && $pn < $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=blog&article='.$url.'&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
}

// This line sets the "LIMIT" range... the 2 values we place to choose a range of rows from database in our query
$limit = 'LIMIT ' .($pn - 1) * $itemsPerPage .',' .$itemsPerPage; 

// Now we are going to run the same query as above but this time add $limit onto the end of the SQL syntax
// $sql2 is what we will use to fuel our while loop statement below
$sql2 = mysql_query("SELECT * FROM gal_blog_coms WHERE com_url = '$geart' ORDER BY com_date ASC $limit"); 

$paginationDisplay = ""; // Initialize the pagination output variable
// This code runs only if the last page variable is ot equal to 1, if it is only 1 page we require no paginated links to display
if ($lastPage != "1"){

    // This shows the user what page they are on, and the total number of pages
    $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage. '&nbsp;  &nbsp;  &nbsp; ';
	
    // If we are not on page 1 we can place the Back button
    if ($pn != 1) {
        $previous = $pn - 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=blog&article='.$url.'&pn=' . $previous . '"> Back</a> ';
    } 
	
    // Lay in the clickable numbers display here between the Back and Next links
    $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
	
    // If we are not on the very last page we can place the Next button
    if ($pn != $lastPage) {
        $nextPage = $pn + 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=blog&article='.$url.'&pn=' . $nextPage . '"> Next</a> ';
    } 
}

// Build the Output Section Here
$cnt = 0;

	 $siteurl = get_gal_conf('set_url');

echo '<table align="center" border="0" width="100%"><tr><td>';
while($row = @mysql_fetch_array($sql2)){ 

	$com_date = $row['com_date'];
	$com_data = $row['com_data'];
        $author = $row['com_author'];
        $c_reg = $row['com_reg'];

        
if($c_reg == 1){
      $query2 = "SELECT profile_avrtr FROM gal_profiles WHERE profile_name = '$author'";
      $result2 = mysql_query($query2);
      $obj2 = mysql_fetch_object($result2);

    @$authoravrtr = $obj2->profile_avrtr;
}else{
    @$authoravrtr = 'default_gal_avrtr.jpg';
}



// outputList
$com_date = date('Y-m-d H:i', $com_date);

echo "<table align='left' border='0' width='100%' class='entry'>
        <tr>
	    <td width='52px' height='52px' class='com_avrtr' valign='top'>
                <img src='".get_gal_conf('file_path')."avrtrs/".$authoravrtr."' width='50px' height='50px' class='avrtrimg'></img>
            </td>
	<td valign='top'>
                <h3 style='margin-left:10px; margin-bottom:5px;'>".$author."</h3>
                <small style='margin-left:10px;'><b>".$com_date."</b></small>";

echo "<hr style='margin-top:5px;'/>
        </td></tr><tr>
        <td width='52px'>
        ";
	
	echo "
    <td>".$com_data."
        <hr style='margin-top:10px'/></div>
        </td>
	</tr>
	</table><div>
    ";
	$cnt++;
    
} // close while loop

echo '</td></tr></table>';
if($nr != 0){
    echo $paginationDisplay;
}else{
    echo 'There are currently no Comments for this Entry. Be the first! ;)';
}
echo '</div>';

################################################################################

echo '<div class="new_comment">
      <h3>Comment this Post:</h3>';

if($commenterror == 0){ echo '<h4 style="color:green;">Thank you for commenting!</h4>'; }
elseif($commenterror == 1){ echo '<h4 style="color:red;">Failed to submit Comment!</h4>'; }

 if($user_name != $ver_user_name OR md5($user_pass) != $ver_user_pass){


    echo '<form action="index.php?c=blog&article='.$url.'&com=new" method="post">
          <table border="0">
          <tr>
          <td align="right">
          <b>Name: </b>
          </td>
          <td align="left" valign="top">
          <input type="text" name="comment_name" size="20" value="';
          if($commenterror == 1){ echo $submitted_name; }
    echo '"></input><font color="red"> <b> *</b></font>
          </td>
          </tr>
          <tr>
          <td colspan="2" valign="top">
          <input type="hidden" name="reg" value="0"></input>
          <textarea id="public" name="comment_text" cols="50" rows="6">';
          if($commenterror == 1){ echo $submitted_text; }
    echo '</textarea></input><font color="red"><b>&nbsp;&nbsp;</b></font>
          </td>
          </tr>
          <tr>
          <td colspan="2" valign="top">
          <input type="submit" value="Submit"></input>
          </td>
          </tr>
          </table>
          </form>';

 } elseif($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass AND $user_level != 4){

     echo '<form action="index.php?c=blog&article='.$url.'&com=new" method="post">
          <table border="0">
          <tr>
          <td colspan="2" valign="top">
          <input type="hidden" name="comment_name" value="'.$user_name.'"></input>
          <input type="hidden" name="reg" value="1"></input>
          <textarea id="public" name="comment_text" cols="50" rows="6"></textarea></input><font color="red"><b>&nbsp;&nbsp;</b></font>
          </td>
          </tr>
          <tr>
          <td colspan="2" valign="top">
          <input type="submit" value="Submit"></input>
          </td>
          </tr>
          </table>
          </form>';
  
 }
 
 echo '</div>';
 
//Add Editor to textarea
  require("./gallery_includes/editorfooter.php");
  
?>
