 <?php
 
/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/
 
 //Load Language-Pack
  require("gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_gal_login.php");   

 echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

	<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
  <head>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>
'.get_gal_conf('set_site_name').' | Register
    </title>';
	
//Load CSS
echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/fonts/icomoon/style.css"/>';  
      echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/themes/gal_default/gal_default_main.css" />';
	
echo '</head>
  <body><div id="container">
<div id="page">
<div id="header">'; 

//Load Header  
require("gallery_includes/themes/gal_default/header.php");

echo '</div>
    <div id="menuhorizontal">';
require 'gallery_includes/themes/gal_default/nav.php';
 echo '</div>
<div id="wrapper">
<div id="content">';
echo '<center>';

$display_accode_form = 1;

if(isset($_GET['accode'])){
    
    $accode = $_GET['accode'];    
    $query_ac = 'UPDATE gal_users SET status = "active" WHERE accode = '.$accode.'';
    
    if(mysql_query($query_ac)){
        echo '<b>Your account was correctly activated!</b><br/>Now, you can login <a href="./index.php?c=gal_login">here!</a>';
        $display_accode_form = 0;
    }else{
        echo 'An Error ocurred while trying to activate this account...';
        $display_accode_form = 1;
    }
    
}

if($display_accode_form == 0){
    echo '';
}else{
    echo '
        <h2 align="center">Activation</h2>
        <form action="index.php" method="get" align="center">
        <input type="hidden" name="c" value="gal_activate"></input>
        <input type="text" name="accode" size="50" value="Activation Code..."></input><br/>
        <input type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Activate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></input>
        </form>
        
<br/>
<b><sup>(?)</sup></b> Insert here the activation Code you <br/>recieved in an Email if the Link in <br/>that Email doesn\'t work.
';
}


   echo '</center></div>';
       
   require 'gallery_includes/themes/gal_default/sidebar.php';

echo '</div>
<div id="footer">';
 
//Load Footer
require("gallery_includes/themes/gal_default/footer.php");

echo '</div>
</div></div>
<small><center>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">MyGallery</a><br/>'.get_gal_conf('gal_version').'</center></small>
  </body>
</html>';


?>
