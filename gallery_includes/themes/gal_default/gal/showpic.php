<?php

//Load Language-Pack
  require("gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_showpic.php");
  
echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

	<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
  <head>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>
      '.get_gal_conf('set_site_name').' | '.$_e["lg_showpic_sitename"].'
    </title>';

//Load CSS
      echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/themes/gal_default/gal_default_main.css" />';
   
   echo '</head><body><div id="container">
<div id="page">
<div id="header">';

//Load Header
include("gallery_includes/themes/gal_default/header.php"); 

echo '</div>
    <div id="menuhorizontal">';
require 'gallery_includes/themes/gal_default/nav.php';
 echo '</div>
<div id="wrapper">
<div id="content">';


if(get_gal_conf('set_order_by') == "DESC"){
$set_order_by = "DESC";
}else{
$set_order_by = "ASC";
}
  


$sql = mysql_query("SELECT * FROM gal_pics ORDER BY pic_date ".$set_order_by."");

///////////////// PAG LOGIK //////////////////////////

$nr = mysql_num_rows($sql); // Get total of Num rows from the database query
if (isset($_GET['pn'])) { // Get pn from URL vars if it is present
    $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']); // filter everything but numbers for security
	
} else { // If the pn URL variable is not present force it to be value of page number 1
    $pn = 1;
} 

//This is where we set how many database items to show on each page 
$itemsPerPage = 1; 

// Get the value of the last page in the pagination result set
$lastPage = ceil($nr / $itemsPerPage);

// Be sure URL variable $pn(page number) is no lower than page 1 and no higher than $lastpage
if ($pn < 1) { // If it is less than 1
    $pn = 1; // force if to be 1
	
} else if ($pn > $lastPage) { // if it is greater than $lastpage
    $pn = $lastPage; // force it to be $lastpage's value
} 

// This creates the numbers to click in between the next and back buttons
$centerPages = "";
$sub1 = $pn - 1;
$sub2 = $pn - 2;
$add1 = $pn + 1;
$add2 = $pn + 2;
if ($pn == 1) {
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
} else if ($pn == $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
} else if ($pn > 2 && $pn < ($lastPage - 1)) {
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
} else if ($pn > 1 && $pn < $lastPage) {
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
    $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
    $centerPages .= '&nbsp; <a href="index.php?c=showpic&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
}

// This line sets the "LIMIT" range... the 2 values we place to choose a range of rows from database in our query
$limit = 'LIMIT ' .($pn - 1) * $itemsPerPage .',' .$itemsPerPage; 

// Now we are going to run the same query as above but this time add $limit onto the end of the SQL syntax
// $sql2 is what we will use to fuel our while loop statement below
$sql2 = mysql_query("SELECT * FROM gal_pics ORDER BY pic_date ".$set_order_by." $limit"); 

$paginationDisplay = ""; // Initialize the pagination output variable
// This code runs only if the last page variable is ot equal to 1, if it is only 1 page we require no paginated links to display
if ($lastPage != "1"){

    // This shows the user what page they are on, and the total number of pages
    //$paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage. '&nbsp;  &nbsp;  &nbsp; ';
	
    // If we are not on page 1 we can place the Back button
    if ($pn != 1) {
        $previous = $pn - 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=showpic&pn=' . $previous . '"> &lt;&lt; Back</a> ';
    } 
	
    // Lay in the clickable numbers display here between the Back and Next links
    $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
	
    // If we are not on the very last page we can place the Next button
    if ($pn != $lastPage) {
        $nextPage = $pn + 1;
        $paginationDisplay .=  '&nbsp;  <a href="index.php?c=showpic&pn=' . $nextPage . '"> Next &gt;&gt;</a> ';
    } 
}

// Build the Output Section Here
$cnt = 0;

echo '<table border="0" align="center">';

while($row = @mysql_fetch_array($sql2)){ 

    $id = $row["pic_id"];
    $name = $row["pic_dat"];

	$title = $row['pic_title'];
	$descr = $row['pic_descr'];
	

echo "<tr><td><img class='one_image' src='".get_gal_conf('file_path')."$name' border='0' style='max-width:".get_gal_conf('set_image_maxwidth')."px;max-height:".get_gal_conf('set_image_maxheight')."px;'></td>";
	
	$cnt++;

	
    if($cnt >= 1)
        {
           echo "</tr>";
		   $cnt = 0;
        }
	
    
} // close while loop
echo '</table><br/>

      <div style="text-align:center;">';
	  
echo $paginationDisplay;
echo "</div>";



//Show Title and Description of the Image  
if(get_gal_conf('show_dats') == "yes"){	   
echo '
<div align="left" style="margin-left:10%; margin-right:10%">
<br/><br/>
   <table width="100%" border="0">
      <tr>
         <td align="right" width="30%" valign="top">
            '.$_e["lg_showpic_title"].'
         </td>
         <td align="left" width="70%" valign="top">
            <font size="5">'.$title.'</font>
         </td>
      </tr>
      <tr>
         <td align="right" width="30%" valign="top">
            '.$_e["lg_showpic_descr"].'
         </td>
         <td align="left" width="70%" valign="top">'.$descr.'</td>
      </tr>
   </table>
</div>
';
}



   echo '</div>';
       
   require 'gallery_includes/themes/gal_default/sidebar.php';

echo '</div>
<div id="footer">';

//Load Footer
require("gallery_includes/themes/gal_default/footer.php");

echo '</div>
</div></div>
<small><center>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">MyGallery</a><br/>'.get_gal_conf('gal_version').'</center></small>
  </body>
</html>';

?>