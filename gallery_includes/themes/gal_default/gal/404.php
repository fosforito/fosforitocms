<?php
session_start();
 
/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/

//Default 404 Error Page - not ready...
//Last modification: 17.07.2013 by Jens Leon Wagner

echo'<html>';
echo'<head>';
echo'<title>Error 404 | Page not found!</title>';
echo'';
echo'';
echo'</head>';
echo'<body>';

echo'<h1>Error 404:</h1>';
echo'<h3>The requested Page doesn\'t exist!</h3>';
echo'<br/>';
echo'<hr/>';
echo'<br/>';
echo'<a href="./index.php">Home</a>';
echo'<div style="text-align:right; ">
    Powered by <a style="color:black; text-decoration:none; 
    "href="http://www.fosforito.net">FosforitoCMS</a></div>';

echo'</body>';
echo'</html>';
echo'';
echo'';

?>
