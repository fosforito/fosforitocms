 <?php
 
/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/
 
//Load Language-Pack
  require("gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_gal_login.php");    

//Check if already a session exists
               if(isset($_POST['user_name'])){
  $_SESSION['user_name'] = $_POST['user_name'];
  $user_name = $_POST['user_name'];
               }
  
               if(isset($_POST['user_pass'])){
  $_SESSION['user_pass'] = $_POST['user_pass'];
  $user_pass = $_POST['user_pass'];
               }
  
               if(!isset($_POST['user_name'])){
  $user_name = "";
               }
  
               if(!isset($_POST['user_pass'])){
  $user_pass = "";
               }
  
//Verificate User Details
require 'gallery_includes/user_ver.php';

  
  if(!isset($_GET['msj'])){
  $_GET['msj'] = "";
  }
  
  if(isset($_GET['redirectto'])){
  $redirectto = $_GET['redirectto'];
  }else{
  $redirectto = "index.php";
  }
  
$_SESSION['user_level'] = $ver_user_level;  
$sredi = 0;

//Redirect if needed...
if($_GET['msj'] == "hello" AND ($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass)){
    if($status == 'active'){
        $sredi = 1;

    }
} else {
    if($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass){
        $sredi = 1;
    }
}

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
      "http://www.w3.org/TR/html4/loose.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" lang="de">
      <head>';

    if($sredi == 1){
        echo'<meta http-equiv="Refresh" content="1; URL='.$redirectto.'">';
    }

echo'<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
          <link rel="icon" href="/favicon.ico" type="image/x-icon">
          <title>
              '.get_gal_conf('set_site_name').' | '.$_e["lg_gal_login_sitename"].'
          </title>';
	
//Load CSS
echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/fonts/icomoon/style.css"/>
     <link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/themes/gal_default/gal_default_main.css" />
     </head>
     <body><div id="container">
     <div id="page">
     <div id="header">'; 

//Load Header  
require'./gallery_includes/themes/gal_default/header.php';

echo '</div>
     <div id="menuhorizontal">';

//Load Navigation
require './gallery_includes/themes/gal_default/nav.php';

echo '</div>
         <div id="wrapper">
             <div id="content">
                 <center>'; 

//Check if Session Data is correct and from an existing User...
if($_GET['msj'] == "hello" AND ($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass)){
    
   //Check if User Account is active (activated via Email)
   if($status == 'active'){
       
       echo '<h3>Hello '.$user_name.'!</h3><fieldset class="fieldset_ok"><span aria-hidden="true" data-icon="&#xe022;"></span> <b>You were successfully<br/>logged in!</b></fieldset>';
       echo $_e['lg_gal_login_redirect'];
       
   //If Account was not activated, display message and reset Session Vars...
   }elseif($status == 'inactive'){
       echo 'Please activate your account first by clicking on the Link recieved in an Email.<br/><a href="'.get_gal_conf('set_url').'/index.php?c=gal_activate">Activate Account now!</a>';
       $_SESSION['user_name'] = '';
       $_SESSION['user_pass'] = '';
   }

} else {
  
if($user_name == $ver_user_name AND md5($user_pass) == $ver_user_pass){
echo $_e["lg_gal_login_alreadyli"];
echo $_e['lg_gal_login_redirect'];

} else {


//If incorrect, view Login-Form
echo $_e["lg_gal_login_logintitle"];

if(isset($_POST['siteid'])){
if(($_POST['user_name'] != $ver_user_name) OR (md5($_POST['user_pass']) != $ver_user_pass)){
echo $_e["lg_gal_login_err"];
}
}

if(isset($_POST['user_name'])){
$varinput = $_POST['user_name'];
} else {
$varinput = "";
}


echo'
<table border="0" align="center">
<form action="index.php?c=gal_login&msj=hello&redirectto='.$redirectto.'" method="post">
<tr><td align="right">';

echo $_e["lg_gal_login_putuser"];

echo '</td><td><input type="text" name="user_name" value="'.$varinput.'"></input><br/></dt></tr>
<tr><td align="right">';

echo $_e["lg_gal_login_putpass"];

echo '</td><td><input type="password" name="user_pass"></input></dt></tr>
<input type="hidden" name="siteid" value="2"></input>
<tr><td colspan="2" align="center"><input type="submit" value="';

echo $_e["lg_gal_login_submit"];

echo '"></td></tr>
</form>
</table>';

}
}
    
   echo '</center></div>';
       
   require 'gallery_includes/themes/gal_default/sidebar.php';

echo '</div>
<div id="footer">';
 
//Load Footer
require("gallery_includes/themes/gal_default/footer.php");

echo '</div>
</div></div>
<small><center>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">MyGallery</a><br/>'.get_gal_conf('gal_version').'</center></small>
  </body>
</html>';


?>