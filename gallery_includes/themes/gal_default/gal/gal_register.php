 <?php
 
/*******************************************************************************
*            ___            _                      ___         ___             *
*     //    |              |           ^  |       |    |\  /| |        \\      *
*    //     |__  __   __  _|_  __   _    _|_  __  |    | \/ | |___      \\     *
*    \\     |   |  | |__   |  |  | |   |  |  |  | |    |    |     |     //     *
*     \\    |   |__|  __|  |  |__| |   |  |_ |__| |___ |    |  ___|    //      *
*                                                                              *
*  @ Copyright by Jens Leon Wagner                                             *
*  This Software can not be selled!                                            *
*  Modify and share it as you like but always with our Copyright-Information!  *
*  Download the latest Version of FosforitoCMS on Fosforito.Net:               *
*  @ http://www.fosforito.net                                                  *
*******************************************************************************/
 
     if(!isset($_SESSION['admin_name'])){
     $_SESSION['admin_name'] = "";
     }
     if(!isset($_SESSION['admin_pass'])){
     $_SESSION['admin_pass'] = "";
     }

$user_name = @$_SESSION['user_name'];
$user_pass = @$_SESSION['user_pass'];
$user_level = @$_SESSION['user_level'];
 
//Verificate User Details
include('gallery_includes/user_ver.php'); 
 
if($user_name != $ver_user_name OR md5($user_pass) != $ver_user_pass){
     $variable1 = 1;
     $variable2 = 1;
     $variable3 = 1;
     $errvar1 = 0;
     $errvar2 = 0;
     $errvar3 = 0;
     
     if(isset($_POST['gal_reg_step'])){
         if($_POST['gal_reg_step'] == "second"){
             
             $varcon_username = $_POST['gal_reg_username'];
             $varcon_email = $_POST['gal_reg_email'];
             
             
             if($_POST['gal_reg_pass1'] == $_POST['gal_reg_pass2']){
                 if($_POST['gal_reg_pass1'] != '' AND $_POST['gal_reg_pass1'] != ' '){
                     $new_reg_pass = $_POST['gal_reg_pass1'];
                     $variable1 = 0;
                 }else{
                     $variable1 = 1;
                     $errvar1 = 1;
                 }
             }else{
                 $variable1 = 1;
                 $errvar1 = 1;
             }
             
             
             if($_POST['gal_reg_username'] == '' OR $_POST['gal_reg_username'] == ' '){
                 $variable2 = 1;
                 $errvar2 = 1;
             }else{
                 $new_reg_username = $_POST['gal_reg_username'];
                 $variable2 = 0;
             }
             
             
             if($_POST['gal_reg_email'] == '' OR $_POST['gal_reg_email'] == ' ' OR $_POST['gal_reg_email'] == '@'){
                 $variable3 = 1;
                 $errvar3 = 1;
             }else{
                 $new_reg_email = $_POST['gal_reg_email'];
                 $variable3 = 0;
             }
             
             
         }else{
             $varcon_username = '';
             $varcon_email = '@';
         }
     }else{
             $varcon_username = '';
             $varcon_email = '@';
     }
 
 
 
if($variable1 == 0 AND $variable2 == 0 AND $variable3 == 0){
    //Add User to Database
    
        $ac_code = time().rand(100000, 999999);
        
        $gal_reg_username = $_POST['gal_reg_username'];
        $gal_reg_email = $_POST['gal_reg_email'];
        $gal_reg_pass = md5($_POST['gal_reg_pass1']);
        
        $query = "INSERT INTO gal_users (user_name, user_email, user_pass, user_level, accode, status, usrprotected) VALUES ('$gal_reg_username', '$gal_reg_email', '$gal_reg_pass', '3', '$ac_code', 'inactive', '0')";
        mysql_query($query);
        
    //End Add User to Database 
    //Mail Section

        $sender = get_gal_conf('set_site_name');
        $empfaenger = "$gal_reg_email";
        $betreff = "".get_gal_conf('set_site_name')." | Account Activation";
        $mailtext = "Hello ".$gal_reg_username."!\n\nThank you for your Registration on ".get_gal_conf('set_site_name')."!\nNow please activate your account, clicking on the Link above and enjoy!\n\n".get_gal_conf('set_url')."/?c=gal_activate&accode=".$ac_code."\n\nThis Email was automatically sent by ".get_gal_conf('set_site_name').".\nIf you don't registered on our Website, please ignore this message.";
        mail($empfaenger, $betreff, $mailtext, "From: $sender ");

     //End of Mail Section
}

}


################################################################################


//Load Language-Pack
  require("gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_gal_login.php");   

 echo '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

	<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
  <head>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>
'.get_gal_conf('set_site_name').' | Register
    </title>';
	
//Load CSS
echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/fonts/icomoon/style.css"/>';  
      echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/themes/gal_default/gal_default_main.css" />';
	
echo '</head>
  <body><div id="container">
<div id="page">
<div id="header">'; 

//Load Header  
require("gallery_includes/themes/gal_default/header.php");

echo '</div>
    <div id="menuhorizontal">';
require 'gallery_includes/themes/gal_default/nav.php';
 echo '</div>
<div id="wrapper">
<div id="content">';
echo '<center>';

if(get_gal_conf('allow_regs') == 1){
if($user_name != $ver_user_name OR md5($user_pass) != $ver_user_pass){
if($variable1 == 1 OR $variable2 == 1 OR $variable3 == 1){
        
    echo '<h2>Registration</h2>';
    
        if($errvar1 == 1 OR $errvar2 == 1 OR $errvar3 == 1){
            echo '<div class="fieldset_error">';
            
                if($errvar1 == 1){
                    echo '<b>ERROR:</b> The Passwords must be the same...<br/>';
                }

                if($errvar2 == 1){
                    echo '<b>ERROR:</b> Insert a valid User Name...<br/>';
                }

                if($errvar3 == 1){
                    echo '<b>ERROR:</b> Insert a valid Email Adress...<br/>';
                }
        
            echo '</div>';
        }
    
    echo'
     <form action="index.php?c=gal_register" method="post">
     <table border="0"> 
     <tr>
     <td align="right">User Name: </td>
     <td>
     <input type="hidden" name="gal_reg_step" value="second"></input>
     <input type="text" name="gal_reg_username" value="'.$varcon_username.'"></input>
     </td>
     </tr>
     <tr>
     <td align="right">Email: </td>
     <td>
     <input type="text" name="gal_reg_email" value="'.$varcon_email.'"></input>
     </td>
     </tr>
     <tr>
     <td align="right">Password: </td>
     <td>
     <input type="password" name="gal_reg_pass1" value="hwi94o"></input>
     </td>
     </tr>
     <tr>
     <td align="right">Repeat Password:</td>
     <td>
     <input type="password" name="gal_reg_pass2" value="cljkk8"></input>
     </td>
     </tr>
     <tr>
     <td colspan="2" align="center">
     <input type="submit" value="Register"></input>
     </td>
     </tr>
     </table>
     </form>
     ';
}elseif($variable1 == 0 AND $variable2 == 0 AND $variable3 == 0){
        
        //ACTIVATED!
        echo '
            <div class="fieldset_ok">
            Your Account was successfully created! <br/>Thank you! Now, please check your <br/>Emails and activate your account by clicking <br/>on
            the link provided within that Email.
            </div>
            ';
        
        
}
    
    
    
 }else{
     echo 'You are already registered! o_O';
 }
 
}else{
    echo 'At the moment, no Registrations are allowed...';
}
 

   echo '</center></div>';
       
   require 'gallery_includes/themes/gal_default/sidebar.php';

echo '</div>
<div id="footer">';
 
//Load Footer
require("gallery_includes/themes/gal_default/footer.php");

echo '</div>
</div></div>
<small><center>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">MyGallery</a><br/>'.get_gal_conf('gal_version').'</center></small>
  </body>
</html>';


?>