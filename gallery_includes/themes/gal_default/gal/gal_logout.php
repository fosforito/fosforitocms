<?php  
session_destroy();
  
//Load Language-Pack
  require("gallery_includes/languages/".get_gal_conf('set_site_language')."/".get_gal_conf('set_site_language')."_lang_gal_logout.php");  

header("refresh: 1; index.php");

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

	<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
  <head>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title>
      '.get_gal_conf('set_site_name').' | '.$_e["lg_gal_logout_sitename"].'
    </title>';
	
//Load CSS
echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/fonts/icomoon/style.css"/>';
      echo '<link rel="stylesheet" type="text/css" href="'.get_gal_conf('set_url').'/gallery_includes/themes/gal_default/gal_default_main.css" />';
	
echo '</head>
  <body><div id="container">
<div id="page">
<div id="header">
';

//Load Header  
require("gallery_includes/themes/gal_default/header.php");

echo '</div>
    <div id="menuhorizontal">';
require 'gallery_includes/themes/gal_default/nav.php';
 echo '</div>
<div id="wrapper">
<div id="content">';
echo '<center>';
echo $_e["lg_gal_logout_logoutmsj"];

   echo '</center></div>';
       
   require 'gallery_includes/themes/gal_default/sidebar.php';

echo '</div>
<div id="footer">';

//Load Footer
require("gallery_includes/themes/gal_default/footer.php");

echo '</div>
</div></div>
<small><center>Created with <a style="color:black; text-decoration:none;" target="_blank" title="Visit in a new Window" href="http://www.fosforito.net">MyGallery</a><br/>'.get_gal_conf('gal_version').'</center></small>
  </body>
</html>';

?>
