<?php
/**
 *  AlberT-cache
 *  fast and portable full-page caching system
 *
 *
 * You have simply to include the main cache filename
 * {@link AlberT-cache.inc.php }, eventually overwriting some
 * configuration variables just <b>before</b> inclusion
 *
 * @copyright Copyleft &copy; Emiliano Gabrielli
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author Emiliano Gabrielli <AlberT@SuperAlberT.it>
 * @version $Id: config.inc.php,v 1.6 2004/12/14 17:38:07 albert Exp $
 * @package AlberT-cache
 */

/**#@+
 * <p>
 * This configuration variable may be overwritten in every page.
 */

/**
 * Whether or not to use GZIP, defaults to 1.
 * @global bool $CACHE_GZIP
 */
if (!isset($CACHE_GZIP))
	$CACHE_GZIP = 1;

/**
 * Whether or not to cache POST, defaults to 1.
 * @global bool $CACHE_POST
 */
if (!isset($CACHE_POST))
	$CACHE_POST = 1;

/**
 * Sets the cache timeout, a value of -1 will disable
 * caching, but still allows gzipping.
 *
 * @global int $CACHE_TIMEOUT
 */
if (!isset($CACHE_TIMEOUT))
	$CACHE_TIMEOUT = NULL;

/**
 * Put here additional variables you want the cache
 * to be based on.
 *
 * This is usefull if you want a different page based on the
 * value of a particular variable not yet in POST or GET.
 * i.e.: your pages are different based on the value of
 * $_SESSION['username'].
 *
 * @global mixed $CACHE_ALSO
 */
if (!isset($CACHE_ALSO))
	$CACHE_ALSO = NULL;

/**#@-*/

/**#@+
 * The value of this variable affects the entire site.
 */

/** Turn debugging on/off, defaults to 0.
 * @global bool $CACHE_DEBUG
 */
$CACHE_DEBUG = 0;

/**
 * Turn caching on/off, defaults to 1.
 * @global bool $CACHE_ON
 */
$CACHE_ON = 1;

/**
 * Set the number of seconds the cache will be valid,
 * defaults to 900 (15 minutes).
 * @global int $CACHE_EXP
 */
$CACHE_EXP = 900;

/**
 * Set if the hostname has to be appended to the cache filename,
 * turn it to 0 in a multi site environment; defaults to 1.
 * @global bool $CACHE_1SITE
 */
$CACHE_1SITE = 1;

/**
 * Set the directory name in which to store cache files.
 * @global string $CACHE_DIR
 */
$CACHE_DIR = dirname(__FILE__).'/cache';

/**
 * Set the probability the Garbage Collector will be run, defaults to 1 (1%)
 * @global bool $CACHE_GC
 */
$CACHE_GC = 1;

/**
 * Set the 'umask' for the cache files.
 * You don't need to change if you don't know what it is :-P
 * @global int $CACHE_UMASK
 */
$CACHE_UMASK = 0022;

/**#@-*/
?>
