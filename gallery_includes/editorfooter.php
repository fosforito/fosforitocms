<?php

echo "<script>
                CKEDITOR.replace( 'simple', {
                    uiColor: '#B9B9B9',
                    height: '150px',
                    width: '450px',
                    toolbar: [
                            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
                            [ 'FontSize', 'TextColor', 'BGColor' ],
                            [ 'Smiley' ]
                    ]
                });
	 </script>";

echo "<script>
                CKEDITOR.replace( 'public', {
                    uiColor: '#B9F1B9',
                    height: '100px',
                    width: '400px',
                    toolbar: [
                            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
                            [ 'FontSize', 'TextColor', 'BGColor' ],
                            [ 'Smiley' ]
                    ]
                });
	 </script>";

echo "<script>
                CKEDITOR.replace( 'advanced', {
                    uiColor: '#B9B9B9',
                    toolbar: [
                            [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ],
                            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
                            [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ],
                            '/',
                            [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ],
                            [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ],
                            [ 'Link', 'Unlink', 'Anchor' ],
                            '/',
                            [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
                            [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ],
                            '/',
                            [ 'Styles', 'Format', 'Font', 'FontSize' ],
                            [ 'TextColor', 'BGColor' ],
                            [ 'Maximize', 'ShowBlocks' ]
                    ]
                });
                
window.CKEDITOR = CKEDITOR;

CKEDITOR.plugins.add( 'pgrfilemanager' );

CKEDITOR.config.filebrowserBrowseUrl = CKEDITOR.basePath+'plugins/pgrfilemanager/PGRFileManager.php',
CKEDITOR.config.filebrowserImageBrowseUrl = CKEDITOR.basePath+'plugins/pgrfilemanager/PGRFileManager.php?type=Image',
CKEDITOR.config.filebrowserFlashBrowseUrl = CKEDITOR.basePath+'plugins/pgrfilemanager/PGRFileManager.php?type=Flash',
CKEDITOR.config.filebrowserUploadUrl = CKEDITOR.basePath+'plugins/pgrfilemanager/PGRFileManager.php?type=Files',
CKEDITOR.config.filebrowserImageUploadUrl = CKEDITOR.basePath+'plugins/pgrfilemanager/PGRFileManager.php?type=Image',
CKEDITOR.config.filebrowserFlashUploadUrl = CKEDITOR.basePath+'plugins/pgrfilemanager/PGRFileManager.php?type=Flash'

	 </script>";

?>
