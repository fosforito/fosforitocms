<?php

$_e = array(
"lg_addpic_sitename" => 'Neues Bild',
"lg_addpic_loginfirst" => '<h3>Du hast keine Erlaubnis um diese Seite zu sehen!</h3>Bitte logge dich erst ein!<br/><a href="../index.php?c=gal_login">Zur Login Seite &gt;&gt;</a>',
"lg_addpic_uploadnew" => '<h2>Neues Bild hochladen</h2>',
"lg_addpic_file" => 'Bild:',
"lg_addpic_text" => 'Titel:',
"lg_addpic_font" => 'Schriftart:',
"lg_addpic_fontsize" => 'Schriftgr��e:',
"lg_addpic_color" => 'Farbe:',
"lg_addpic_textposition" => 'Schriftposition (x/y):',
"lg_addpic_bordertype" => 'Rahmenart:',
"lg_addpic_submit" => 'Hochladen',
"lg_addpic_status" => 'Status:',
"lg_addpic_top" => 'Oben',
"lg_addpic_bottom" => 'Unten',
"lg_addpic_left" => 'Links',
"lg_addpic_right" => 'Rechts',
"lg_addpic_descr" => 'Beschreibung:'

);

?>