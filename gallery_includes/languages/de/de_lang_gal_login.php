 <?php

$_e = array(
"lg_gal_login_sitename" => 'Login',
"lg_gal_login_alreadyli" => 'Du bist schon eingeloggt!',
"lg_gal_login_logintitle" => '<h2>Login</h2>',
"lg_gal_login_err" => '<fieldset class="fieldset_error"><span aria-hidden="true" data-icon="&#xe040;"></span> <b>FEHLER: Benutzername oder Passwort falsch!</b></fieldset>',
"lg_gal_login_putuser" => '<b>Benutzername:</b>',
"lg_gal_login_putpass" => '<b>Passwort:</b>',
"lg_gal_login_submit" => 'Einloggen',
"lg_gal_login_li" => '<h3>Hallo Admin!</h3><fieldset class="fieldset_ok"><span aria-hidden="true" data-icon="&#xe022;"></span> <b>Du wurdest erfolgreich<br/>eingeloggt!</b></fieldset>',
"lg_gal_login_redirect" => '<br/><br/><i>Leite weiter...</i>'
);

?>