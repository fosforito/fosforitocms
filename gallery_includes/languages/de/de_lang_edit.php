<?php
$_e = array(
"lg_edit_sitename" => 'Bild editieren',
"lg_edit_loginfirst" => '<h3>Du hast keine Erlaubnis um diese Seite zu sehen!</h3>Bitte logge dich erst ein!<br/><a href="gal_login.php">Zur Login Seite &gt;&gt;</a>',
"lg_edit_title" => 'Titel',
"lg_edit_descr" => 'Beschreibung',
"lg_edit_status" => 'Status',
"lg_edit_klicks" => 'Klicks',
"lg_edit_submit" => 'Aktualisieren',
"lg_edit_sitetitle" => '<h2>Bild editieren</h2>',
"lg_edit_sitename" => 'Datenbank aktualisieren',
"lg_edit_ready" => '<h4 style="color:green;">Bild erfolgreich editiert!</h4>',
"lg_edit_reedit" => '&lt;&lt; Zur�ck zum Editieren',
"lg_edit_viewimage" => 'Bild ansehen &gt;&gt;'
);
?>