<?php

$_e = array(
"lg_addpic_sitename" => 'Nueva Im&aacute;gen',
"lg_addpic_loginfirst" => '<h3>No tienes permisos para ver &eacute;sta p&aacute;gina!</h3>Por favor ingresa primero!<br/><a href="../index.php?c=gal_login">Ingresar &gt;&gt;</a>',
"lg_addpic_uploadnew" => '<h2>Subir nueva Im&aacute;gen</h2>',
"lg_addpic_file" => 'Im&aacute;gen:',
"lg_addpic_text" => 'T&iacute;tulo:',
"lg_addpic_font" => 'Tipo de Letra:',
"lg_addpic_fontsize" => 'Tama&ntilde;o de Letra:',
"lg_addpic_color" => 'Color:',
"lg_addpic_textposition" => 'Posici&oacute;n de la letra (x/y):',
"lg_addpic_bordertype" => 'Tipo de Borde:',
"lg_addpic_submit" => 'Subir',
"lg_addpic_status" => 'Estado:',
"lg_addpic_top" => 'Arriba',
"lg_addpic_bottom" => 'Abajo',
"lg_addpic_left" => 'Izquierda',
"lg_addpic_right" => 'Derecha',
"lg_addpic_descr" => 'Descripci&oacute;n:'

);

?>