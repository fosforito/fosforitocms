<?php

$_e = array(
"lg_addpic_script_sitename" => 'Subir Im&aacute;gen',
"lg_addpic_script_extensionerror" => 'Tipo de Archivo no permitido<br/>',
"lg_addpic_script_overmaxsize" => 'Archivo demasiado grande.<br/>',
"lg_addpic_script_uploaded" => 'Archivo subido.<br/>',
"lg_addpic_script_notuploaded" => 'No se pudo subir el archivo!<br/>',
"lg_addpic_script_addedtodb" => 'Archivo agregado a la Base de Datos.<br/>',
"lg_addpic_script_notaddedtodb" => 'No se pudo agregar el Archivo a la Base de Datos!<br/>',
"lg_addpic_script_loaded" => 'Archivo cargado con &eacute;xito.<br/>',
"lg_addpic_script_notloaded" => 'No se pudo abrir el Archivo!<br/>',
"lg_addpic_script_getsize" => 'Tama&ntilde;os leidos.<br/>',
"lg_addpic_script_getsizefailed" => 'No se ha podido leer los Tama&ntilde;os del Archivo!<br/>',
"lg_addpic_script_thumbok" => 'Thumbnail creado.<br/>',
"lg_addpic_script_thumbnn" => 'Thymbnail no creado!<br/>',
"lg_addpic_script_thumbd" => 'Thumbnail guardado.<br/>',
"lg_addpic_script_thumbn" => 'Thumbnail no guardado!<br/>',
"lg_addpic_script_bordok" => 'Im�gen guardada con Borde.<br/>',
"lg_addpic_script_bord" => 'Im&aacute;gen guardada sin Borde.<br/>',
"lg_addpic_script_links" => '<br/><b><a href="index.php?c=galimages.php&action=add">&lt;&lt; Subir m&aacute;s Imagenes </a> | <a href="../index.php?c=showgalerie">Ir a la Galer&iacute;a &gt;&gt;</a></b></center>',
"lg_addpic_script_loginfirst" => '<h3>No tienes permisos para ver �sta p�gina</h3>Por favor, ingresa primero<br/>
<a href="../index.php?c=gal_login">Ingresar &gt;&gt;</a>'

);

?>