 <?php

$_e = array(
"lg_gal_login_sitename" => 'Login',
"lg_gal_login_alreadyli" => 'Ya est�s ingresado!',
"lg_gal_login_logintitle" => '<h2>Login</h2>',
"lg_gal_login_err" => '<fieldset class="fieldset_error"><span aria-hidden="true" data-icon="&#xe040;"></span> <b>ERROR: Nombre de Usuario o Contrase�a incorrecto!</b></fieldset>',
"lg_gal_login_putuser" => '<b>Nombre de Usuario:</b>',
"lg_gal_login_putpass" => '<b>Contrase�a:</b>',
"lg_gal_login_submit" => 'Ingresar',
"lg_gal_login_li" => '<h3>Hola Admin!</h3><fieldset class="fieldset_ok"><span aria-hidden="true" data-icon="&#xe022;"></span> <b>Iniciaste sesi�n<br/>con �xito!</b></fieldset>',
"lg_gal_login_redirect" => '<br/><br/><i>Redireccionando...</i>'
);

?>