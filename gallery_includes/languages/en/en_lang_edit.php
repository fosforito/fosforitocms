<?php
$_e = array(
"lg_edit_sitename" => 'Edit Image',
"lg_edit_loginfirst" => '<h3>You don\'t have Permission to access this Site!</h3>Please Login first!<br/>
<a href="gal_login.php">Go to Login Page &gt;&gt;</a>',
"lg_edit_title" => 'Title',
"lg_edit_descr" => 'Description',
"lg_edit_status" => 'Status',
"lg_edit_klicks" => 'Klicks',
"lg_edit_submit" => 'Update',
"lg_edit_sitetitle" => '<h2>Edit Image</h2>',
"lg_edit_sitename" => 'Update Database',
"lg_edit_ready" => '<h4 style="color:green;">Image edited successfully!</h4>',
"lg_edit_reedit" => '&lt;&lt; Go back to Edit',
"lg_edit_viewimage" => 'View Image &gt;&gt;'
);
?>