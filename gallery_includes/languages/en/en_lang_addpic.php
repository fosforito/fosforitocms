<?php

$_e = array(
"lg_addpic_sitename" => 'New Image',
"lg_addpic_loginfirst" => '<h3>You don\'t have Permission to access this Site!</h3>Please Login first!<br/><a href="../index.php?c=gal_login">Go to Login Page &gt;&gt;</a>',
"lg_addpic_uploadnew" => '<h2>Upload new Image</h2>',
"lg_addpic_file" => 'Picture:',
"lg_addpic_text" => 'Titel:',
"lg_addpic_font" => 'Font',
"lg_addpic_fontsize" => 'Font Size',
"lg_addpic_color" => 'Color',
"lg_addpic_textposition" => 'Text Position (x/y):',
"lg_addpic_bordertype" => 'Bordertype:',
"lg_addpic_submit" => 'Upload',
"lg_addpic_status" => 'Status:',
"lg_addpic_top" => 'Top',
"lg_addpic_bottom" => 'Bottom',
"lg_addpic_left" => 'Left',
"lg_addpic_right" => 'Right',
"lg_addpic_descr" => 'Description:'

);

?>