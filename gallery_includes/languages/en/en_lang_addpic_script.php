<?php

$_e = array(
"lg_addpic_script_sitename" => 'Image Upload',
"lg_addpic_script_extensionerror" => 'Filetype not allowed<br/>',
"lg_addpic_script_overmaxsize" => 'Maximal Filesize passed.<br/>',
"lg_addpic_script_uploaded" => 'File uploaded.<br/>',
"lg_addpic_script_notuploaded" => 'File not uploaded!<br/>',
"lg_addpic_script_addedtodb" => 'File was saved in Database.<br/>',
"lg_addpic_script_notaddedtodb" => 'File not saved in Database!<br/>',
"lg_addpic_script_loaded" => 'File loaded.<br/>',
"lg_addpic_script_notloaded" => 'File not loaded!<br/>',
"lg_addpic_script_getsize" => 'Size successfully readed.<br/>',
"lg_addpic_script_getsizefailed" => 'Can not read size of Image!<br/>',
"lg_addpic_script_thumbok" => 'Thumbnail created.<br/>',
"lg_addpic_script_thumbnn" => 'Thymbnail not created!<br/>',
"lg_addpic_script_thumbd" => 'Thumbnail saved.<br/>',
"lg_addpic_script_thumbn" => 'Thumbnail not saved!<br/>',
"lg_addpic_script_bordok" => 'Picture saved with Border.<br/>',
"lg_addpic_script_bord" => 'Picture saved without Border.<br/>',
"lg_addpic_script_links" => '<br/><b><a href="index.php?c=galimages.php&action=add">&lt;&lt; Upload more Pictures </a> | <a href="../index.php?c=showgalerie">Go to the Gallery &gt;&gt;</a></b></center>',
"lg_addpic_script_loginfirst" => '<h3>You don\'t have Permission to access this Site!</h3>Please Login first!<br/>
<a href="../index.php?c=gal_login">Go to Login Page &gt;&gt;</a>'

);

?>