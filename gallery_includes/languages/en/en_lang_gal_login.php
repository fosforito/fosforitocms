 <?php

$_e = array(
"lg_gal_login_sitename" => 'Login',
"lg_gal_login_alreadyli" => 'You are already logged in!',
"lg_gal_login_logintitle" => '<h2>Login</h2>',
"lg_gal_login_err" => '<fieldset class="fieldset_error"><span aria-hidden="true" data-icon="&#xe040;"></span> <b>ERROR: Username or Password incorrect!</b></fieldset>',
"lg_gal_login_putuser" => '<b>Username:</b>',
"lg_gal_login_putpass" => '<b>Password:</b>',
"lg_gal_login_submit" => 'Login',
"lg_gal_login_li" => '<h3>Hello Admin!</h3><fieldset class="fieldset_ok"><span aria-hidden="true" data-icon="&#xe022;"></span> <b>You were successfully<br/>logged in!</b></fieldset>',
"lg_gal_login_redirect" => '<br/><br/><i>Redirecting...</i>'
);

?>